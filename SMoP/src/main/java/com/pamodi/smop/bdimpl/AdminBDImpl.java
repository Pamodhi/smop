package com.pamodi.smop.bdimpl;

import org.json.JSONArray;

import com.pamodi.smop.bd.AdminBD;
import com.pamodi.smop.dao.AdminDAO;

public class AdminBDImpl implements AdminBD {
	private AdminDAO adminDAO;

	public AdminDAO getAdminDAO() {
		return adminDAO;
	}

	public void setAdminDAO(AdminDAO adminDAO) {
		this.adminDAO = adminDAO;
	}

	public String confirmUserRegistration(int userId, String status)throws Exception {
		return getAdminDAO().confirmUserRegistration(userId,status);
	}

	public JSONArray getUserList() throws Exception {
		return getAdminDAO().getUserList() ;
	}
}
