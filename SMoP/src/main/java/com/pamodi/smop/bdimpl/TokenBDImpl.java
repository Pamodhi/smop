package com.pamodi.smop.bdimpl;

import com.pamodi.smop.bd.TokenBD;
import com.pamodi.smop.dao.TokenDAO;

public class TokenBDImpl implements TokenBD{
	
	private TokenDAO tokenDAO;
	public TokenDAO getTokenDAO() {
		return tokenDAO;
	}
	public void setTokenDAO(TokenDAO tokenDAO) {
		this.tokenDAO = tokenDAO;
	}
	public boolean isTokenExist(String token, int userId) throws Exception {
		return getTokenDAO().isTokenExist(token, userId);
	}

}
