package com.pamodi.smop.bdimpl;

import org.json.JSONArray;

import com.pamodi.smop.bd.BillPaymentBD;
import com.pamodi.smop.dao.BillPaymentDAO;
import com.pamodi.smop.domain.PaymentInfo;
import com.pamodi.smop.dto.PaymentInfoDTO;

import net.sf.json.JSONObject;

public class BillPaymentBDImpl implements BillPaymentBD {
	private BillPaymentDAO billPaymentDAO;
	
	public BillPaymentDAO getBillPaymentDAO() {
		return billPaymentDAO;
	}

	public void setBillPaymentDAO(BillPaymentDAO billPaymentDAO) {
		this.billPaymentDAO = billPaymentDAO;
	}

	public JSONArray getSubcategory() throws Exception {
		return getBillPaymentDAO().getSubcategory();
	}

	@Override
	public JSONObject getMonthlyBill(int userId) throws Exception {
		return getBillPaymentDAO().getMonthlyBill(userId);
	}

	@Override
	public PaymentInfo createBill(PaymentInfoDTO payInfo) {
		return getBillPaymentDAO().createBill(payInfo);
	}

	public void deleteBill(int id) throws Exception {
		getBillPaymentDAO().deleteBill(id);
	}

	@Override
	public JSONArray getbillDetails(int userId) throws Exception {
		return getBillPaymentDAO().getbillDetails( userId);
	}

	@Override
	public void payBill(int id) throws Exception {
		getBillPaymentDAO().payBill(id);
	}
	
	
	
}
