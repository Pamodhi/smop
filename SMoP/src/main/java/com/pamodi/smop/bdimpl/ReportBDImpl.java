package com.pamodi.smop.bdimpl;

import java.util.Date;

import org.json.JSONArray;

import com.pamodi.smop.bd.ReportBD;
import com.pamodi.smop.dao.ReportDAO;

public class ReportBDImpl implements ReportBD{
	private ReportDAO reportDAO;

	public ReportDAO getReportDAO() {
		return reportDAO;
	}

	public void setReportDAO(ReportDAO reportDAO) {
		this.reportDAO = reportDAO;
	}

	@Override
	public JSONArray getBudget(Date toDate, Date fromDate, int userId) throws Exception {
		return getReportDAO().getBudget(toDate, fromDate, userId);
	}

	@Override
	public JSONArray getAccountTrans(Date toDate, Date fromDate, int userId) throws Exception {
		return getReportDAO().getAccountTrans(toDate, fromDate, userId);
	}

	@Override
	public JSONArray getExpenses(Date toDate, Date fromDate, int userId) throws Exception {
		return getReportDAO().getExpenses(toDate, fromDate, userId);
	}

	@Override
	public JSONArray getIncome(Date toDate, Date fromDate, int userId) throws Exception {
		return getReportDAO().getIncome(toDate, fromDate, userId);
	}
	
	

}
