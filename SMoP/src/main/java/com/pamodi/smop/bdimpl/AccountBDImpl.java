package com.pamodi.smop.bdimpl;

import org.json.JSONArray;

import com.pamodi.smop.bd.AccountBD;
import com.pamodi.smop.dao.AccountDAO;
import com.pamodi.smop.domain.AccountTransaction;
import com.pamodi.smop.domain.Accounts;
import com.pamodi.smop.dto.AccountDTO;
import com.pamodi.smop.dto.TransactionDTO;

import net.sf.json.JSONObject;

public class AccountBDImpl implements AccountBD {
	
	private AccountDAO accountDAO;

	public AccountDAO getAccountDAO() {
		return accountDAO;
	}
	public void setAccountDAO(AccountDAO accountDAO) {
		this.accountDAO = accountDAO;
	}

	@Override
	public Accounts createAccount(AccountDTO accountDTO) throws Exception {
		return getAccountDAO().createAccount(accountDTO);
	}
	@Override
	public JSONArray getAccounts(int userId) throws Exception {
		return getAccountDAO().getAccounts(userId);
	}
	@Override
	public AccountTransaction createAccountTrans(String type, Accounts account) throws Exception {
		return getAccountDAO().createAccountTrans(type, account);
	}
	public AccountTransaction createAccountTrans(AccountTransaction account, int userId) throws Exception {
		return getAccountDAO().createAccountTrans(account, userId);
	}
	@Override
	public JSONObject getAccountBalance(int userId) throws Exception {
		return getAccountDAO().getAccountBalance(userId);
	}
	public void deleteAccountTrans(int tarnsId) throws Exception {
		getAccountDAO().deleteAccountTrans(tarnsId);
		
	}
	@Override
	public void updateAccountTrans(int tarnsId, TransactionDTO account) throws Exception {
		getAccountDAO().updateAccountTrans(tarnsId, account);
		
	}

}
