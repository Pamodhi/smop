package com.pamodi.smop.bdimpl;

import org.json.JSONArray;

import com.pamodi.smop.bd.FutureGoalBD;
import com.pamodi.smop.dao.FutureGoalDAO;
import com.pamodi.smop.domain.FutureGoal;
import com.pamodi.smop.dto.FutureGoalDTO;

public class FutureGoalBDImpl implements FutureGoalBD {
	
	private FutureGoalDAO futureGoalDAO;
	public FutureGoalDAO getFutureGoalDAO() {
		return futureGoalDAO;
	}
	public void setFutureGoalDAO(FutureGoalDAO futureGoalDAO) {
		this.futureGoalDAO = futureGoalDAO;
	}

	public FutureGoal createGoal(FutureGoalDTO goalDTO) throws Exception {
		return getFutureGoalDAO().createGoal(goalDTO);
	}
	@Override
	public JSONArray getGoalDetails(int userId) throws Exception {
		return getFutureGoalDAO().getGoalDetails(userId);
	}

}
