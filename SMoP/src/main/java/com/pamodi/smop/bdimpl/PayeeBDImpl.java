package com.pamodi.smop.bdimpl;

import org.json.JSONArray;

import com.pamodi.smop.bd.PayeeBD;
import com.pamodi.smop.dao.PayeeDAO;
import com.pamodi.smop.domain.Payee;
import com.pamodi.smop.dto.PayeeDTO;

public class PayeeBDImpl implements PayeeBD {

	private PayeeDAO payeeDAO;

	public PayeeDAO getPayeeDAO() {
		return payeeDAO;
	}

	public void setPayeeDAO(PayeeDAO payeeDAO) {
		this.payeeDAO = payeeDAO;
	}

	public Payee createPayee(PayeeDTO payee) throws Exception {
		return getPayeeDAO().createPayee(payee);
	}

	public JSONArray getPayee(int userId) throws Exception {
		return getPayeeDAO().getPayee(userId);
	}

	public JSONArray getPayeeTransDetails(int id) throws Exception {
		return getPayeeDAO().getPayeeTransDetails(id);
	}
	
}
