package com.pamodi.smop.bdimpl;

import org.json.JSONArray;

import com.pamodi.smop.bd.UserRegistrationBD;
import com.pamodi.smop.dao.UserRegistrationDAO;
import com.pamodi.smop.domain.UserDetails;
import com.pamodi.smop.domain.UserLoginDetails;
import com.pamodi.smop.dto.UserDetailsDTO;
import com.pamodi.smop.dto.UserLoginDTO;

import net.sf.json.JSONObject;

public class UserRegistrationBDImpl implements UserRegistrationBD {
	
	private UserRegistrationDAO userRegistrationDAO;
	
	public UserRegistrationDAO getUserRegistrationDAO() {
		return userRegistrationDAO;
	}

	public void setUserRegistrationDAO(UserRegistrationDAO userRegistrationDAO) {
		this.userRegistrationDAO = userRegistrationDAO;
	}

	public Integer insertProfile(UserDetailsDTO userDetailsDTO) throws Exception {
		return getUserRegistrationDAO().insertProfile(userDetailsDTO) ;
	}
	public boolean checkUserExist(String nic) throws Exception {
		return getUserRegistrationDAO().checkUserExist(nic);
	}

	public Integer checkEmailExist(String email) throws Exception {
		return getUserRegistrationDAO().checkEmailExist(email);
	}

	public UserDetails verifyCode(int code, String email) throws Exception {
		return getUserRegistrationDAO().verifyCode(code, email);
	}

	public boolean verifyUser(int userId) throws Exception {
		return getUserRegistrationDAO().verifyUser(userId);
	}

	@Override
	public JSONArray getProfession() throws Exception {
		return getUserRegistrationDAO().getProfession();
	}

	@Override
	public JSONArray getCurrency() throws Exception {
		return getUserRegistrationDAO().getCurrency();
	}

	@Override
	public Integer getcode(int userId) throws Exception {
		return getUserRegistrationDAO().getcode(userId);
	}

	@Override
	public JSONObject getUserDetails(int userId) throws Exception {
		return getUserRegistrationDAO().getUserDetails(userId);
	}
	

}
