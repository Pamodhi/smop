package com.pamodi.smop.bdimpl;

import com.pamodi.smop.bd.LoginBD;
import com.pamodi.smop.dao.LoginDAO;
import com.pamodi.smop.domain.UserLoginDetails;
import com.pamodi.smop.dto.UserLoginDTO;

public class LoginBDImpl implements LoginBD {
	
	private LoginDAO loginDAO;
	public LoginDAO getLoginDAO() {
		return loginDAO;
	}
	public void setLoginDAO(LoginDAO loginDAO) {
		this.loginDAO = loginDAO;
	}
	public UserLoginDetails getUserLoginDetail(String userEmail, String password) throws Exception{
		return getLoginDAO().getUserLoginDetail(userEmail, password) ;
		
	}
	public UserLoginDetails insertLoginDetails(UserLoginDTO userloginDTO)throws Exception{
		return getLoginDAO().insertLoginDetails(userloginDTO);
		
	}
	public boolean checkUserExist(String email) throws Exception {
		return getLoginDAO().checkUserExist(email);
	}
	public boolean updateToken(String email, String authenticationToken) throws Exception {
		return getLoginDAO().updateToken(email, authenticationToken);
	}
	@Override
	public int userType(int userId) throws Exception {
		return getLoginDAO().userType(userId);
	}
	@Override
	public boolean updatePassword(String password, String email) throws Exception {
		return getLoginDAO().updatePassword(password, email);
	}

}
