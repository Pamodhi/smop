package com.pamodi.smop.bdimpl;

import org.json.JSONArray;

import com.pamodi.smop.bd.FamilySyncBD;
import com.pamodi.smop.dao.FamilySyncDAO;
import com.pamodi.smop.domain.FamilySync;
import com.pamodi.smop.dto.FamilySyncDTO;

public class FamilySyncDBImpl implements FamilySyncBD{
	
	private FamilySyncDAO familySyncDAO;
	public FamilySyncDAO getFamilySyncDAO() {
		return familySyncDAO;
	}

	public void setFamilySyncDAO(FamilySyncDAO familySyncDAO) {
		this.familySyncDAO = familySyncDAO;
	}
	@Override
	public FamilySync createGroup(FamilySyncDTO family) throws Exception {
		return getFamilySyncDAO().createGroup(family);
	}

	@Override
	public boolean checkgroupExist(String code, String name, int userId) throws Exception {
		return getFamilySyncDAO().checkgroupExist(code, name, userId);
	}

	@Override
	public JSONArray getAlltransactions(int userId) throws Exception {
		return getFamilySyncDAO().getAlltransactions(userId);
	}

}
