package com.pamodi.smop.bdimpl;

import org.json.JSONArray;

import com.pamodi.smop.bd.IncomeBD;
import com.pamodi.smop.dao.ExpenseDAO;
import com.pamodi.smop.dao.IncomeDAO;
import com.pamodi.smop.domain.TransactionDetails;
import com.pamodi.smop.dto.TransactionDTO;

import net.sf.json.JSONObject;

public class IncomeBDImpl implements IncomeBD {
	private IncomeDAO incomeDAO;

	public IncomeDAO getIncomeDAO() {
		return incomeDAO;
	}

	public void setIncomeDAO(IncomeDAO incomeDAO) {
		this.incomeDAO = incomeDAO;
	}

	public TransactionDetails createIncomeDetails(TransactionDTO transactionDTO) throws Exception {
		return getIncomeDAO().createIncomeDetails(transactionDTO);
	}

	public void updateIncomeDetails(TransactionDTO transactionDTO) throws Exception {
		getIncomeDAO().updateIncomeDetails(transactionDTO);
	}

	public void deleteIncomeDetails(int id) throws Exception {
		getIncomeDAO().deleteIncomeDetails(id);
	}

	public JSONObject getIncomeDetails(int transId,int userId) throws Exception {
		return getIncomeDAO().getIncomeDetails(transId,userId);
	}

	public JSONArray getIncomeDetails(int userId) throws Exception {
		return getIncomeDAO().getIncomeDetails(userId);
	}

	@Override
	public JSONObject getMonthlyIncome(int userId) throws Exception {
		return getIncomeDAO().getMonthlyIncome(userId);
	}

}
