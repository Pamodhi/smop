package com.pamodi.smop.bdimpl;

import java.util.Date;

import org.json.JSONArray;

import com.pamodi.smop.bd.ExpenseBD;
import com.pamodi.smop.dao.ExpenseDAO;
import com.pamodi.smop.domain.PaymentInfo;
import com.pamodi.smop.domain.TransactionDetails;
import com.pamodi.smop.dto.PaymentInfoDTO;
import com.pamodi.smop.dto.TransactionDTO;

import net.sf.json.JSONObject;

public class ExpenseBDImpl implements ExpenseBD {
	private ExpenseDAO expenseDAO;

	
	public ExpenseDAO getExpenseDAO() {
		return expenseDAO;
	}
	public void setExpenseDAO(ExpenseDAO expenseDAO) {
		this.expenseDAO = expenseDAO;
	}
	public TransactionDetails createExpenseDetails(TransactionDTO transactionDTO) throws Exception {
		return getExpenseDAO().createExpenseDetails(transactionDTO);
	}
	public void updateExpenseDetails(TransactionDTO transactionDTO) throws Exception {
		getExpenseDAO().updateExpenseDetails(transactionDTO);
		
	}
	public void deleteExpenseDetails(int id) throws Exception {
		getExpenseDAO().deleteExpenseDetails(id);
		
	}
	public JSONObject getExpenseDetails(int id, int userId) throws Exception {
		return getExpenseDAO().getExpenseDetails(id, userId);
	}
	public JSONArray getExpenseDetails(int userId) throws Exception {
		return getExpenseDAO().getExpenseDetails(userId);
	}
	public JSONArray getSubcategory() throws Exception {
		return getExpenseDAO().getSubcategory();
	}
	@Override
	public JSONObject getMonthlyExpese(int userId) throws Exception {
		return getExpenseDAO().getMonthlyExpese(userId);
	}
	@Override
	public JSONArray getTransactionDateRange(Date toDate, Date fromDate, int userId) throws Exception {
		return getExpenseDAO().getTransactionDateRange(toDate, fromDate, userId);
	}
	@Override
	public double getLastMonthExpense(int catId, int userId) throws Exception {
		return getExpenseDAO().getLastMonthExpense(catId, userId);
	}
	@Override
	public void updateFutureGoal(int userId) throws Exception {
		getExpenseDAO().updateFutureGoal(userId);
	}
	
	
}
