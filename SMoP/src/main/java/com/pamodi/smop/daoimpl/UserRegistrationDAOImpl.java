package com.pamodi.smop.daoimpl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;

import com.pamodi.smop.dao.UserRegistrationDAO;
import com.pamodi.smop.domain.CurrencyType;
import com.pamodi.smop.domain.UserDetails;
import com.pamodi.smop.domain.UserLoginDetails;
import com.pamodi.smop.domain.UserProfession;
import com.pamodi.smop.dto.UserDetailsDTO;

import net.sf.json.JSONObject;

@Transactional
public class UserRegistrationDAOImpl implements UserRegistrationDAO {
	
	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public Integer insertProfile(UserDetailsDTO userDetailsDTO) {
		
		UserDetails userDetails = new UserDetails();
		userDetails.setFristName(userDetailsDTO.getFname());
		userDetails.setLastName(userDetailsDTO.getLname());
		userDetails.setNic(userDetailsDTO.getNic());
		userDetails.setUserEmail(userDetailsDTO.getEmail());
		userDetails.setCode(userDetailsDTO.getCode());
		userDetails.setSts("P");
		getSession().save(userDetails);
		
		String query ="SELECT MAX(userId) FROM UserDetails";
		Query a = getSession().createQuery(query);
		int userId = (Integer) a.uniqueResult();
		
		return userId;
	}
	
	public JSONArray getProfession() {
		JSONArray object =new JSONArray();
		
		String profession = "SELECT prof FROM UserProfession prof";
		Query professioQuery = getSession().createQuery(profession);
		
		List<UserProfession> list = professioQuery.list();
		for (UserProfession dataObject : list) {
			JSONArray profObject =new JSONArray();
			profObject.put(dataObject.getProffessionId());
			profObject.put(dataObject.getDescription());
			object.put(profObject);
		}
		return object;
		
	}
	
	public JSONArray getCurrency() {
		JSONArray object =new JSONArray();
		
		String currency = "SELECT crncy FROM CurrencyType crncy";
		Query currencyQuery = getSession().createQuery(currency);
		
		List<CurrencyType> list1 = currencyQuery.list();
		for (CurrencyType dataObject1 : list1) {
			JSONArray crncyObject =new JSONArray();
			crncyObject.put(dataObject1.getCurrencyId());
			crncyObject.put(dataObject1.getCurrencyType());
			object.put(crncyObject);
		}
		return object;
		
	}

	public boolean checkUserExist(String nic) {
		Criteria user= getSession().createCriteria(UserDetails.class);
		user.add(Restrictions.eq("nic", nic));
		
		UserDetails userDetails = (UserDetails) user.uniqueResult();
		
		if(userDetails != null) {
			return true;
		}else {
			return false;
		}
	}
	
	public Integer checkEmailExist(String email) {
		Criteria user= getSession().createCriteria(UserDetails.class);
		user.add(Restrictions.eq("userEmail", email));
		
		UserDetails userDetails = (UserDetails) user.uniqueResult();
		
		if(userDetails != null) {
			return userDetails.getUserId();
		}else {
			return 0;
		}
	}

	@Override
	public UserDetails verifyCode(int code, String email) {
		Criteria user= getSession().createCriteria(UserDetails.class);
		user.add(Restrictions.eq("userEmail", email));
		user.add(Restrictions.eq("code", code));
		UserDetails userDetails = (UserDetails) user.uniqueResult();
		
		if(userDetails != null) {
			return userDetails;
		}else {
			return null;
		}
	}
	
	
	public boolean verifyUser(int userId) {
		Criteria userlog= getSession().createCriteria(UserLoginDetails.class);
		userlog.add(Restrictions.eq("userId", userId));
		UserLoginDetails userLoginDetails = (UserLoginDetails) userlog.uniqueResult();
		
		userLoginDetails.setIsVerify(1);
		getSession().update(userLoginDetails);
		return true;
		
	}

	@Override
	public Integer getcode(int userId) {
		Criteria user= getSession().createCriteria(UserDetails.class);
		user.add(Restrictions.eq("userId", userId));
		UserDetails userDetails = (UserDetails) user.uniqueResult();
		
		if(userDetails != null) {
			return userDetails.getCode();
		}else {
			return 0;
		}
	}

	@Override
	public JSONObject getUserDetails(int userId) {
		Criteria user= getSession().createCriteria(UserDetails.class);
		user.add(Restrictions.eq("userId", userId));
		UserDetails userDetails = (UserDetails) user.uniqueResult();
		
		JSONObject object =new JSONObject();
		object.put("id",userDetails.getUserId());
		object.put("fname",userDetails.getFristName());
		object.put("lanme",userDetails.getLastName());
		object.put("nic",userDetails.getNic());
		object.put("email",userDetails.getUserEmail());
		object.put("prof","Teacher");
		object.put("curncy","LRK");
		
		return object;
	}

}
