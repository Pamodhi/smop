package com.pamodi.smop.daoimpl;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.pamodi.smop.dao.LoginDAO;
import com.pamodi.smop.domain.UserDetails;
import com.pamodi.smop.domain.UserLoginDetails;
import com.pamodi.smop.dto.UserLoginDTO;
@Transactional
public class LoginDAOImpl implements LoginDAO {

	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public UserLoginDetails getUserLoginDetail(String userEmail, String password) {
		
		Criteria login= getSession().createCriteria(UserLoginDetails.class);
		login.add(Restrictions.eq("email", userEmail));
		login.add(Restrictions.eq("userPassword", password));
		
		UserLoginDetails userLoginDetails = (UserLoginDetails) login.uniqueResult();
		
		return userLoginDetails;
		
	}
	
	public UserLoginDetails insertLoginDetails(UserLoginDTO userloginDTO) {
		
		UserLoginDetails userLog = new UserLoginDetails();
		userLog.setUserName(userloginDTO.getUserName());
		userLog.setEmail(userloginDTO.getEmail());
		userLog.setUserPassword(userloginDTO.getUserPassword());
		userLog.setUserAccessTypeId(userloginDTO.getUserAccessTypeId());
		userLog.setUserId(userloginDTO.getUserId());
		
		getSession().save(userLog);
		
		return userLog;
	}
	
	public void createUser(UserLoginDetails userLog) {
		UserDetails user = new UserDetails();
		//user.setUserLogingId(userLog.getLoginId());
		getSession().save(user);
	}

	public boolean checkUserExist(String email) {
		Criteria userlog= getSession().createCriteria(UserLoginDetails.class);
		userlog.add(Restrictions.eq("email", email));
		
		UserLoginDetails userLoginDetails = (UserLoginDetails) userlog.uniqueResult();
		
		if(userLoginDetails != null) {
			return true;
		}else {
			return false;
		}
	}
	
	public boolean updateToken(String email , String authenticationToken) {
		Criteria userlog= getSession().createCriteria(UserLoginDetails.class);
		userlog.add(Restrictions.eq("email", email));
		UserLoginDetails userLoginDetails = (UserLoginDetails) userlog.uniqueResult();
		
		userLoginDetails.setToken(authenticationToken);
		getSession().update(userLoginDetails);
		
		return true;
		
	}

	@Override
	public int userType(int userId) {
		Criteria login= getSession().createCriteria(UserLoginDetails.class);
		login.add(Restrictions.eq("userId", userId));
		UserLoginDetails userLoginDetails = (UserLoginDetails) login.uniqueResult();
		return userLoginDetails.getUserAccessTypeId() ;
	}

	@Override
	public boolean updatePassword(String password, String email) {
		Criteria userlog= getSession().createCriteria(UserLoginDetails.class);
		userlog.add(Restrictions.eq("email", email));
		UserLoginDetails userLoginDetails = (UserLoginDetails) userlog.uniqueResult();
		
		userLoginDetails.setUserPassword(password);
		getSession().update(userLoginDetails);
		return true;
	}
	
}
