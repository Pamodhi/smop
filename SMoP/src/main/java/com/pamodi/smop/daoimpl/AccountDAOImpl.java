package com.pamodi.smop.daoimpl;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;

import com.pamodi.smop.dao.AccountDAO;
import com.pamodi.smop.domain.AccountTransaction;
import com.pamodi.smop.domain.Accounts;
import com.pamodi.smop.domain.TransactionDetails;
import com.pamodi.smop.dto.AccountDTO;
import com.pamodi.smop.dto.TransactionDTO;

import net.sf.json.JSONObject;
@Transactional
public class AccountDAOImpl implements AccountDAO {
	
	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public Accounts createAccount(AccountDTO accountDTO) {
		
		Accounts accounts = new Accounts();
		accounts.setUserId(accountDTO.getUserId());
		accounts.setAccountNo(accountDTO.getAccountNo());
		accounts.setAccountName(accountDTO.getAccountName());
		accounts.setAccountBalance(accountDTO.getAccountBalance());
		accounts.setCurrencyId(accountDTO.getCurrencyId());
		
		getSession().save(accounts);
		return accounts;
		
	}

	@Override
	public JSONArray getAccounts(int userId) {
		Criteria accountDetails= getSession().createCriteria(Accounts.class);
		accountDetails.add(Restrictions.eq("userId",userId));
		List<Accounts> data = accountDetails.list();
		
		JSONArray obj =new JSONArray();
		for (Accounts dataObject : data) {
			JSONObject object =new JSONObject();
			object.put("acc_no", dataObject.getAccountName());
			object.put("acc_id", dataObject.getAccountId());
			obj.put(object);
		}
		return obj;
	}
	
	public AccountTransaction createAccountTrans(String type, Accounts account) {
		
		AccountTransaction accountTrans = new AccountTransaction();
		accountTrans.setAccountId(account.getAccountId());
		accountTrans.setAmount(account.getAccountBalance());
		accountTrans.setCurrentAmount(account.getAccountBalance());
		accountTrans.setTransType(type);
		
		getSession().save(accountTrans);
		return accountTrans;
		
	}

	@Override
	public AccountTransaction createAccountTrans(AccountTransaction accountTrans, int userId) {
		Criteria accountDetails= getSession().createCriteria(Accounts.class);
		accountDetails.add(Restrictions.eq("userId",userId));
		accountDetails.add(Restrictions.eq("accountId",accountTrans.getAccountId()));
		Accounts data = (Accounts) accountDetails.uniqueResult();
		
		double balance = data.getAccountBalance();
		double currentBalance =0.00;
		if(accountTrans.getTransType() =="DR") {
			currentBalance= balance - accountTrans.getAmount();
		}else {
			currentBalance= balance + accountTrans.getAmount();
		}
		
		accountTrans.setCurrentAmount(currentBalance);
		getSession().save(accountTrans);
		
		data.setAccountBalance(currentBalance);
		getSession().update(data);
		return null;
	}

	@Override
	public JSONObject getAccountBalance(int userId) {
		
		String query ="SELECT SUM(accountBalance) FROM Accounts WHERE userId=:userId";
		Query monthExp = getSession().createQuery(query);
		monthExp.setInteger("userId", userId);
		Object sumAmount =  monthExp.uniqueResult();
		
		JSONObject object=new JSONObject();
		if(sumAmount!=null) {
			object.put("accountBalance", sumAmount);
		}else {
			object.put("accountBalance", sumAmount);
		}
		return object;
	}

	@Override
	public void deleteAccountTrans(int tarnsId) {
		Criteria transactionDetails= getSession().createCriteria(TransactionDetails.class);
		transactionDetails.add(Restrictions.eq("transactionId",tarnsId));
		TransactionDetails transDetails =(TransactionDetails) transactionDetails.uniqueResult();
		
		if(transDetails.getAccountId() >0) {
			Criteria accountDetails= getSession().createCriteria(Accounts.class);
			accountDetails.add(Restrictions.eq("userId",transDetails.getUserId()));
			accountDetails.add(Restrictions.eq("accountId",transDetails.getAccountId()));
			Accounts data = (Accounts) accountDetails.uniqueResult();
			
			double balance = data.getAccountBalance();
			double currentBalance =0.00;
			
			Date sysDate =null;
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");  
			   LocalDateTime now = LocalDateTime.now();
			   sysDate = Date.from(now.atZone(ZoneId.systemDefault()).toInstant());
			
			AccountTransaction accountTrans = new AccountTransaction();
			accountTrans.setAmount(transDetails.getAmount());
			accountTrans.setAccountId(transDetails.getAccountId());
			if(transDetails.getTransactionType()=="INC") {
				accountTrans.setTransType("DR");
				currentBalance =balance - transDetails.getAmount();
			}else if(transDetails.getTransactionType()=="EXP") {
				accountTrans.setTransType("CR");
				currentBalance =balance + transDetails.getAmount();
			}
			
			accountTrans.setTransDate(sysDate);
			accountTrans.setCurrentAmount(currentBalance);
			getSession().save(accountTrans);
			
			data.setAccountBalance(currentBalance);
			getSession().update(data);
		}
		
	}

	@Override
	public void updateAccountTrans(int tarnsId, TransactionDTO account) {
		Criteria transactionDetails= getSession().createCriteria(TransactionDetails.class);
		transactionDetails.add(Restrictions.eq("transactionId",tarnsId));
		TransactionDetails transDetails =(TransactionDetails) transactionDetails.uniqueResult();
		
		Criteria accountDetails= getSession().createCriteria(Accounts.class);
		accountDetails.add(Restrictions.eq("userId",transDetails.getUserId()));
		accountDetails.add(Restrictions.eq("accountId",transDetails.getAccountId()));
		Accounts data = (Accounts) accountDetails.uniqueResult();
		
		double newAmount = account.getAmount();
		double currntAmount = transDetails.getAmount();
		
		double difference = 0.00;
		double currentBalance = 0.00;
		
		if(newAmount != currntAmount) {
			
			Date sysDate =null;
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");  
			   LocalDateTime now = LocalDateTime.now();
			   sysDate = Date.from(now.atZone(ZoneId.systemDefault()).toInstant());
			
			AccountTransaction accountTrans = new AccountTransaction();
			
			if(newAmount > 0) {
				difference = newAmount-currntAmount;
				if(transDetails.getTransactionType()=="INC") {
					accountTrans.setTransType("CR");
				}else if(transDetails.getTransactionType()=="EXP") {
					accountTrans.setTransType("DR");
				}
			}else {
				difference = currntAmount- newAmount;
				if(transDetails.getTransactionType()=="INC") {
					accountTrans.setTransType("DR");
				}else if(transDetails.getTransactionType()=="EXP") {
					accountTrans.setTransType("CR");
				}
			}
			
			accountTrans.setAmount(difference);
			accountTrans.setAccountId(transDetails.getAccountId());
			accountTrans.setTransDate(sysDate);
			accountTrans.setCurrentAmount(difference);
			getSession().save(accountTrans);
			
			data.setAccountBalance(currentBalance);
			getSession().update(data);
			
		}
	}
	
	public boolean checkUserSTS(int userId) {
		
		return false;
		
	}

}
