package com.pamodi.smop.daoimpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;

import com.pamodi.smop.dao.IncomeDAO;
import com.pamodi.smop.domain.Accounts;
import com.pamodi.smop.domain.TransactionDetails;
import com.pamodi.smop.dto.TransactionDTO;

import net.sf.json.JSONObject;
@Transactional
public class IncomeDAOImpl implements IncomeDAO {
	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public TransactionDetails createIncomeDetails(TransactionDTO transactionDTO) {
		TransactionDetails transactionDetails =new TransactionDetails();
		transactionDetails.setUserId(transactionDTO.getUserId());
		transactionDetails.setAmount(transactionDTO.getAmount());
		transactionDetails.setDescription(transactionDTO.getDescription());
		transactionDetails.setTransactionType(transactionDTO.getTransactionType());
		transactionDetails.setTransactionDate(transactionDTO.getTransactionDate());
		transactionDetails.setAccountId(transactionDTO.getAccountId());
		transactionDetails.setSts(1);
		
		getSession().save(transactionDetails);
		return transactionDetails;
	}

	public void updateIncomeDetails(TransactionDTO transactionDTO) {
		Criteria transactionDetails= getSession().createCriteria(TransactionDetails.class);
		transactionDetails.add(Restrictions.eq("transactionId",transactionDTO.getTransactionId()));
		TransactionDetails transDetails =(TransactionDetails) transactionDetails.uniqueResult();
		
		transDetails.setAmount(transactionDTO.getAmount());
		transDetails.setTransactionDate(transactionDTO.getTransactionDate());
		transDetails.setDescription(transactionDTO.getDescription());
		
		getSession().update(transDetails);
		
	}

	public void deleteIncomeDetails(int id) {
		Criteria transactionDetails= getSession().createCriteria(TransactionDetails.class);
		transactionDetails.add(Restrictions.eq("transactionId",id));
		TransactionDetails transDetails =(TransactionDetails) transactionDetails.uniqueResult();
		
		getSession().delete(transDetails);
		
	}

	public JSONObject getIncomeDetails(int transId, int userId) {
		Criteria transactionDetails= getSession().createCriteria(TransactionDetails.class);
		transactionDetails.add(Restrictions.eq("userId",userId));
		transactionDetails.add(Restrictions.eq("transactionId",transId));
		transactionDetails.add(Restrictions.eq("transactionType","INC"));
		TransactionDetails transDetails =(TransactionDetails) transactionDetails.uniqueResult();
		
		
		JSONObject object =new JSONObject();
		object.put("id", transDetails.getTransactionId());
		object.put("date",formatDate(transDetails.getTransactionDate()));
		object.put("des", transDetails.getDescription());
		object.put("amount", transDetails.getAmount());
		object.put("acc_id", transDetails.getAccountId());
		if(transDetails.getAccountId()>0) {
			Criteria account= getSession().createCriteria(Accounts.class);
			account.add(Restrictions.eq("accountId",transDetails.getAccountId()));
			Accounts accDetails =(Accounts) account.uniqueResult();
			object.put("acc_no", accDetails.getAccountName());
		}else {
			object.put("acc_no","");
		}
		
		
		
		return object;
	}

	public JSONArray getIncomeDetails(int userId) {
		
		Date sysDate =null;
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");  
		   LocalDateTime now = LocalDateTime.now();
		   sysDate = Date.from(now.atZone(ZoneId.systemDefault()).toInstant());
		
		Date startDate =getDate(1,sysDate);
		Date endDate =getDate(31,sysDate);
		
		Criteria transactionDetails= getSession().createCriteria(TransactionDetails.class);
		transactionDetails.add(Restrictions.eq("userId",userId));
		transactionDetails.add(Restrictions.eq("transactionType","INC"));
		transactionDetails.add(Restrictions.le("transactionDate",endDate));
		transactionDetails.add(Restrictions.ge("transactionDate",startDate));
		List<TransactionDetails> data = transactionDetails.list();
		
		JSONArray obj =new JSONArray();
		for (TransactionDetails dataObject : data) {
			JSONObject object =new JSONObject();
			object.put("id", dataObject.getTransactionId());
			object.put("date", formatDate(dataObject.getTransactionDate()));
			object.put("des", dataObject.getDescription());
			object.put("amount", dataObject.getAmount());
			obj.put(object);
		}
		
		return obj;
	}
	
	public String formatDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String newDate =sdf.format(date);
		
		return newDate;
		
	}

	@Override
	public JSONObject getMonthlyIncome(int userId) {
		Date sysDate =null;
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");  
		   LocalDateTime now = LocalDateTime.now();
		   sysDate = Date.from(now.atZone(ZoneId.systemDefault()).toInstant());
		
		Date startDate =getDate(1,sysDate);
		Date endDate =getDate(31,sysDate);
		
		String query ="SELECT SUM(amount) FROM TransactionDetails WHERE transactionDate>=:startDate AND transactionDate<=:endDate AND transactionType=:transactionType AND userId=:userId ";
		Query monthExp = getSession().createQuery(query);
		monthExp.setDate("startDate", startDate);
		monthExp.setDate("endDate", endDate);
		monthExp.setInteger("userId", userId);
		monthExp.setString("transactionType", "INC");
		Object sumAmount =  monthExp.uniqueResult();
		
		JSONObject object=new JSONObject();
		if(sumAmount !=null) {
			object.put("SumAmount", sumAmount);
		}else {
			object.put("SumAmount", 0);
		}
		
		return object;
	}
	
	public Date getDate(int dayTo, Date sysDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(sysDate);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH)+1;
		//int day = cal.get(Calendar.DAY_OF_MONTH);
		
		String strDate = year+"/"+month+"/"+dayTo;
		Date date = null;
		try {
			date = new SimpleDateFormat("yyyy/MM/dd").parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return date;
		
	}

}
