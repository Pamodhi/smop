package com.pamodi.smop.daoimpl;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.pamodi.smop.dao.TokenDAO;
import com.pamodi.smop.domain.UserLoginDetails;

@Transactional
public class TokenDAOImpl implements TokenDAO {
	
	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public boolean isTokenExist(String token, int userId) {
		Criteria userlog= getSession().createCriteria(UserLoginDetails.class);
		userlog.add(Restrictions.eq("token", token));
		userlog.add(Restrictions.eq("userId", userId));
		UserLoginDetails userLoginDetails = (UserLoginDetails) userlog.uniqueResult();
		
		if(userLoginDetails != null) {
			return true;
		}else {
			return false;
		}
		
	}

}
