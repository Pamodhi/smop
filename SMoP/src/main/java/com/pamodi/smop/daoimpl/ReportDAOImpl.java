package com.pamodi.smop.daoimpl;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;

import com.pamodi.smop.dao.ReportDAO;
import com.pamodi.smop.domain.AccountTransaction;
import com.pamodi.smop.domain.TransactionDetails;

import net.sf.json.JSONObject;

@Transactional
public class ReportDAOImpl implements ReportDAO {
	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public JSONArray getBudget(Date toDate, Date fromDate, int userId) {
		Criteria transactionDetails= getSession().createCriteria(TransactionDetails.class);
		transactionDetails.add(Restrictions.eq("userId",userId));
		transactionDetails.add(Restrictions.le("transactionDate",toDate));
		transactionDetails.add(Restrictions.ge("transactionDate",fromDate));
		transactionDetails.add(Restrictions.eq("sts",1));
		List<TransactionDetails> data =transactionDetails.list();
		
		JSONArray obj =new JSONArray();
		for (TransactionDetails dataObject : data) {
			JSONObject object =new JSONObject();
			object.put("id", dataObject.getTransactionId());
			object.put("date", formatDate(dataObject.getTransactionDate()));
			object.put("des", dataObject.getDescription());
			object.put("amount", dataObject.getAmount());
			object.put("type", dataObject.getTransactionType());
			obj.put(object);
		}
		
		return obj;
	}
	
	public String formatDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String newDate =sdf.format(date);
		
		return newDate;
		
	}

	@Override
	public JSONArray getAccountTrans(Date toDate, Date fromDate, int accountId) {
		Criteria accountDetails= getSession().createCriteria(AccountTransaction.class);
		accountDetails.add(Restrictions.eq("accountId",accountId));
		accountDetails.add(Restrictions.le("transDate",toDate));
		accountDetails.add(Restrictions.ge("transDate",fromDate));
		List<AccountTransaction> data =accountDetails.list();
		
		JSONArray obj =new JSONArray();
		for (AccountTransaction dataObject : data) {
			JSONObject object =new JSONObject();
			object.put("id", dataObject.getAccountTransId());
			object.put("date", formatDate(dataObject.getTransDate()));
			object.put("amount", dataObject.getAmount());
			object.put("type", dataObject.getTransType());
			object.put("balance", dataObject.getCurrentAmount());
			obj.put(object);
		}
		
		return obj;
	}

	@Override
	public JSONArray getExpenses(Date toDate, Date fromDate, int userId) {
		Criteria transactionDetails= getSession().createCriteria(TransactionDetails.class);
		transactionDetails.add(Restrictions.eq("userId",userId));
		transactionDetails.add(Restrictions.le("transactionDate",toDate));
		transactionDetails.add(Restrictions.ge("transactionDate",fromDate));
		transactionDetails.add(Restrictions.eq("transactionType","EXP"));
		transactionDetails.add(Restrictions.eq("sts",1));
		List<TransactionDetails> data =transactionDetails.list();
		
		JSONArray obj =new JSONArray();
		for (TransactionDetails dataObject : data) {
			JSONObject object =new JSONObject();
			object.put("id", dataObject.getTransactionId());
			object.put("date", formatDate(dataObject.getTransactionDate()));
			object.put("des", dataObject.getDescription());
			object.put("amount", dataObject.getAmount());
			object.put("type", dataObject.getTransactionType());
			obj.put(object);
		}
		
		return obj;
	}

	@Override
	public JSONArray getIncome(Date toDate, Date fromDate, int userId) {
		Criteria transactionDetails= getSession().createCriteria(TransactionDetails.class);
		transactionDetails.add(Restrictions.eq("userId",userId));
		transactionDetails.add(Restrictions.le("transactionDate",toDate));
		transactionDetails.add(Restrictions.ge("transactionDate",fromDate));
		transactionDetails.add(Restrictions.eq("transactionType","INC"));
		transactionDetails.add(Restrictions.eq("sts",1));
		List<TransactionDetails> data =transactionDetails.list();
		
		JSONArray obj =new JSONArray();
		for (TransactionDetails dataObject : data) {
			JSONObject object =new JSONObject();
			object.put("id", dataObject.getTransactionId());
			object.put("date", formatDate(dataObject.getTransactionDate()));
			object.put("des", dataObject.getDescription());
			object.put("amount", dataObject.getAmount());
			object.put("type", dataObject.getTransactionType());
			obj.put(object);
		}
		
		return obj;
	}

}
