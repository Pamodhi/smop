package com.pamodi.smop.daoimpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;

import com.pamodi.smop.dao.BillPaymentDAO;
import com.pamodi.smop.domain.CategoryType;
import com.pamodi.smop.domain.PaymentInfo;
import com.pamodi.smop.domain.TransactionDetails;
import com.pamodi.smop.dto.PaymentInfoDTO;
import com.pamodi.smop.dto.TransactionDTO;

import net.sf.json.JSONObject;
@Transactional
public class BillPaymentDAOImpl implements BillPaymentDAO {
	
	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public JSONArray getSubcategory() {
		
		Criteria userDetails= getSession().createCriteria(CategoryType.class);
		List<CategoryType> data = userDetails.list();
		
		JSONArray obj =new JSONArray();
		for (CategoryType dataObject : data) {
			JSONArray userObject =new JSONArray();
			userObject.put(dataObject.getCategoryId());
			userObject.put(dataObject.getDescription());
			obj.put(userObject);
		}
		
		return obj;
	}
	
	public PaymentInfo createBill(PaymentInfoDTO payInfo) {
		Date date =null;
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");  
		   LocalDateTime now = LocalDateTime.now();
		   date = Date.from(now.atZone(ZoneId.systemDefault()).toInstant());
		
		PaymentInfo payDetails = new PaymentInfo();
		payDetails.setDueDate(payInfo.getDueDate());
		payDetails.setAmount(payInfo.getAmount());
		payDetails.setPayeeId(payInfo.getPayeeId());
		payDetails.setStartDate(date);
		payDetails.setUserId(payInfo.getUserId());
		getSession().save(payDetails);	
		
		TransactionDetails transactionDetails =new TransactionDetails();
		transactionDetails.setUserId(payInfo.getUserId());
		transactionDetails.setAmount(payInfo.getAmount());
		transactionDetails.setDescription(payInfo.getDescription());
		transactionDetails.setTransactionType("EXP");
		transactionDetails.setSubcategoryId(payInfo.getSubcategoryId());
		transactionDetails.setPayeeId(payInfo.getPayeeId());
		transactionDetails.setAccountId(payInfo.getAccountId());
		transactionDetails.setPayInfoId(payDetails.getPayId());
		getSession().save(transactionDetails);
		
		return payDetails;
	}
	
	public Integer createPayInfo(PaymentInfoDTO paymentDetails ) {
		
		Date sysDate =null;
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");  
		   LocalDateTime now = LocalDateTime.now();
		   sysDate = Date.from(now.atZone(ZoneId.systemDefault()).toInstant());
		
		PaymentInfo payDetails = new PaymentInfo();
		payDetails.setAmount(paymentDetails.getAmount());
		payDetails.setDueDate(paymentDetails.getDueDate());
		payDetails.setPayeeId(paymentDetails.getPayeeId());
		payDetails.setStartDate(sysDate);
		payDetails.setUserId(paymentDetails.getUserId());
		getSession().save(payDetails);
		
		return 1;
		
	}
	
	public String formatDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String newDate =sdf.format(date);
		
		return newDate;
		
	}

	@Override
	public JSONObject getMonthlyBill(int userId) {
		Date sysDate =null;
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");  
		   LocalDateTime now = LocalDateTime.now();
		   sysDate = Date.from(now.atZone(ZoneId.systemDefault()).toInstant());
		
		Date startDate =getDate(1,sysDate);
		Date endDate =getDate(31,sysDate);
		
		String query ="SELECT SUM(amount) FROM PaymentInfo WHERE startDate>=:startDate AND startDate<=:endDate AND userId=:userId ";
		Query monthExp = getSession().createQuery(query);
		monthExp.setDate("startDate", startDate);
		monthExp.setDate("endDate", endDate);
		monthExp.setInteger("userId", userId);
		Object sumAmount =  monthExp.uniqueResult();
		
		JSONObject object=new JSONObject();
		if(sumAmount !=null) {
			object.put("SumAmount", sumAmount);
		}else {
			object.put("SumAmount", 0);
		}
		
		return object;
	}
	
	public Date getDate(int dayTo, Date sysDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(sysDate);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH)+1;
		//int day = cal.get(Calendar.DAY_OF_MONTH);
		
		String strDate = year+"/"+month+"/"+dayTo;
		Date date = null;
		try {
			date = new SimpleDateFormat("yyyy/MM/dd").parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return date;
		
	}

	@Override
	public void deleteBill(int id) {
		Criteria payInfor= getSession().createCriteria(PaymentInfo.class);
		payInfor.add(Restrictions.eq("payId",id));
		PaymentInfo transDetails =(PaymentInfo) payInfor.uniqueResult();
		
		getSession().delete(transDetails);
		
	}

	@Override
	public JSONArray getbillDetails(int userId) {
		Criteria payInfor= getSession().createCriteria(PaymentInfo.class);
		payInfor.add(Restrictions.eq("userId",userId));
		List<PaymentInfo> data = payInfor.list();
		
		JSONArray obj =new JSONArray();
		for (PaymentInfo dataObject : data) {
			JSONObject object =new JSONObject();
			object.put("id", dataObject.getPayId());
			object.put("dueDate", formatDate(dataObject.getDueDate()));
			object.put("createDate", formatDate(dataObject.getStartDate()));
			object.put("amount", dataObject.getAmount());
			obj.put(object);
		}
		return obj;
	}

	@Override
	public void payBill(int id) {
		Criteria trans= getSession().createCriteria(TransactionDetails.class);
		trans.add(Restrictions.eq("payInfoId",id));
		TransactionDetails transDetails =(TransactionDetails) trans.uniqueResult();
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");  
		   LocalDateTime now = LocalDateTime.now();
		 Date  sysDate = Date.from(now.atZone(ZoneId.systemDefault()).toInstant());
		
		transDetails.setSts(1);
		transDetails.setTransactionDate(sysDate);
		getSession().update(transDetails);
		
		Criteria payInfor= getSession().createCriteria(PaymentInfo.class);
		payInfor.add(Restrictions.eq("payId",id));
		PaymentInfo paymentInfo =(PaymentInfo) payInfor.uniqueResult();
		
		paymentInfo.setSts(1);
		paymentInfo.setPaidDate(sysDate);
		getSession().update(paymentInfo);
		
	}
}
