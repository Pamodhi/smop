package com.pamodi.smop.daoimpl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;

import com.pamodi.smop.dao.AdminDAO;
import com.pamodi.smop.domain.CurrencyType;
import com.pamodi.smop.domain.Profession;
import com.pamodi.smop.domain.UserDetails;
import com.pamodi.smop.domain.UserLoginDetails;

@Transactional
public class AdminDAOImpl implements AdminDAO {
	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public String confirmUserRegistration(int userId, String status) {
		
		UserDetails userDetails =(UserDetails) getSession().get(UserDetails.class, userId);
		userDetails.setSts(status);
		getSession().update(userDetails);
		
		
	  if(status == "A") { 
		  UserLoginDetails userLogDetails = (UserLoginDetails) getSession().get(UserLoginDetails.class, userId);
		  userLogDetails.setIsRegistered(1); 
		  getSession().update(userLogDetails); 
	  }
		 
		
		return status;
	}

	public JSONArray getUserList() {
		Criteria userDetails= getSession().createCriteria(UserDetails.class);
		userDetails.add(Restrictions.eq("sts", "P"));
		List<UserDetails> data = userDetails.list();
		
		JSONArray obj =new JSONArray();
		if(data !=null) {
			for (UserDetails dataObject : data) {
				JSONObject userObject =new JSONObject();
				userObject.put("userId",dataObject.getUserId());
				userObject.put("fName",dataObject.getFristName());
				userObject.put("lName",dataObject.getLastName());
				userObject.put("nic",dataObject.getNic());
				userObject.put("prof","Teacher");
				userObject.put("crncy","LRK");
				
				userObject.put("email",dataObject.getUserEmail());
				obj.put(userObject);
			}
		}
		return obj;
	}
	
	

}
