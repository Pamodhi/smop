package com.pamodi.smop.daoimpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.swing.text.html.parser.DTD;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.json.JSONArray;

import com.pamodi.smop.dao.ExpenseDAO;
import com.pamodi.smop.domain.CategoryType;
import com.pamodi.smop.domain.FutureGoal;
import com.pamodi.smop.domain.PaymentInfo;
import com.pamodi.smop.domain.TransactionDetails;
import com.pamodi.smop.dto.PaymentInfoDTO;
import com.pamodi.smop.dto.TransactionDTO;

import net.sf.json.JSONObject;
@Transactional
public class ExpenseDAOImpl implements ExpenseDAO {
	
	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public TransactionDetails createExpenseDetails(TransactionDTO transactionDTO) {
		
		TransactionDetails transactionDetails =new TransactionDetails();
		transactionDetails.setUserId(transactionDTO.getUserId());
		transactionDetails.setAmount(transactionDTO.getAmount());
		transactionDetails.setDescription(transactionDTO.getDescription());
		transactionDetails.setTransactionType(transactionDTO.getTransactionType());
		transactionDetails.setTransactionDate(transactionDTO.getTransactionDate());
		transactionDetails.setPayeeId(transactionDTO.getPayeeId());
		transactionDetails.setSubcategoryId(transactionDTO.getSubcategoryId());
		transactionDetails.setSts(1);
		transactionDetails.setAccountId(transactionDTO.getAccountId());
		getSession().save(transactionDetails);
		
		return transactionDetails;
	}

	public JSONArray getSubcategory() {
		
		Criteria userDetails= getSession().createCriteria(CategoryType.class);
		List<CategoryType> data = userDetails.list();
		
		JSONArray obj =new JSONArray();
		for (CategoryType dataObject : data) {
			JSONObject userObject =new JSONObject();
			userObject.put("id", dataObject.getCategoryId());
			userObject.put("des", dataObject.getDescription());
			obj.put(userObject);
		}
		
		return obj;
	}

	public void updateExpenseDetails(TransactionDTO transactionDTO) {
		Criteria transactionDetails= getSession().createCriteria(TransactionDetails.class);
		transactionDetails.add(Restrictions.eq("transactionId",transactionDTO.getTransactionId()));
		TransactionDetails transDetails =(TransactionDetails) transactionDetails.uniqueResult();
		
		transDetails.setAmount(transactionDTO.getAmount());
		transDetails.setTransactionDate(transactionDTO.getTransactionDate());
		transDetails.setDescription(transactionDTO.getDescription());
		
		getSession().update(transDetails);
		
	}

	public void deleteExpenseDetails(int id) {
		Criteria transactionDetails= getSession().createCriteria(TransactionDetails.class);
		transactionDetails.add(Restrictions.eq("transactionId",id));
		TransactionDetails transDetails =(TransactionDetails) transactionDetails.uniqueResult();
		
		getSession().delete(transDetails);
		
	}

	public JSONObject getExpenseDetails(int id, int userId) {
		Criteria transactionDetails= getSession().createCriteria(TransactionDetails.class);
		transactionDetails.add(Restrictions.eq("transactionId",id));
		transactionDetails.add(Restrictions.eq("userId",userId));
		TransactionDetails transDetails =(TransactionDetails) transactionDetails.uniqueResult();
		
		JSONObject object =new JSONObject();
		object.put("id", transDetails.getTransactionId());
		object.put("date",formatDate(transDetails.getTransactionDate()));
		object.put("des", transDetails.getDescription());
		object.put("amount", transDetails.getAmount());
		object.put("payee", transDetails.getPayeeId());
		object.put("account", transDetails.getAccountId());
		object.put("category", transDetails.getSubcategoryId());
		
		return object;
	}

	public JSONArray getExpenseDetails(int userId) {
		
		Date sysDate =null;
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");  
		   LocalDateTime now = LocalDateTime.now();
		   sysDate = Date.from(now.atZone(ZoneId.systemDefault()).toInstant());
		
		Date startDate =getDate(1,sysDate);
		Date endDate =getDate(31,sysDate);
		
		Criteria transactionDetails= getSession().createCriteria(TransactionDetails.class);
		transactionDetails.add(Restrictions.eq("transactionType","EXP"));
		transactionDetails.add(Restrictions.eq("userId",userId));
		transactionDetails.add(Restrictions.eq("sts",1));
		transactionDetails.add(Restrictions.le("transactionDate",endDate));
		transactionDetails.add(Restrictions.ge("transactionDate",startDate));
		
		List<TransactionDetails> data = transactionDetails.list();
		
		JSONArray obj =new JSONArray();
		for (TransactionDetails dataObject : data) {
			JSONObject object =new JSONObject();
			object.put("id", dataObject.getTransactionId());
			object.put("date", formatDate(dataObject.getTransactionDate()));
			object.put("des", dataObject.getDescription());
			object.put("amount", dataObject.getAmount());
			obj.put(object);
		}
		
		return obj;
	}
	
	public String formatDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String newDate =sdf.format(date);
		
		return newDate;
		
	}

	@Override
	public JSONObject getMonthlyExpese(int userId) {
		Date sysDate =null;
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");  
		   LocalDateTime now = LocalDateTime.now();
		   sysDate = Date.from(now.atZone(ZoneId.systemDefault()).toInstant());
		
		Date startDate =getDate(1,sysDate);
		Date endDate =getDate(31,sysDate);
		
		String query ="SELECT SUM(amount) FROM TransactionDetails WHERE transactionDate>=:startDate AND transactionDate<=:endDate AND transactionType=:transactionType AND userId=:userId ";
		Query monthExp = getSession().createQuery(query);
		monthExp.setDate("startDate", startDate);
		monthExp.setDate("endDate", endDate);
		monthExp.setInteger("userId", userId);
		monthExp.setString("transactionType", "EXP");
		Object sumAmount =  monthExp.uniqueResult();
		
		JSONObject object=new JSONObject();
		if(sumAmount !=null) {
			object.put("SumAmount", sumAmount);
		}else {
			object.put("SumAmount", 0);
		}
		
		return object;
	}
	
	public Date getDate(int dayTo, Date sysDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(sysDate);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH)+1;
		//int day = cal.get(Calendar.DAY_OF_MONTH);
		
		String strDate = year+"/"+month+"/"+dayTo;
		Date date = null;
		try {
			date = new SimpleDateFormat("yyyy/MM/dd").parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return date;
		
	}
	
	public JSONArray getTransactionDateRange(Date toDate, Date fromDate, int userId) {
		
		Criteria transactionDetails= getSession().createCriteria(TransactionDetails.class);
		transactionDetails.add(Restrictions.eq("transactionType","EXP"));
		transactionDetails.add(Restrictions.eq("userId",userId));
		transactionDetails.add(Restrictions.le("transactionDate",toDate));
		transactionDetails.add(Restrictions.ge("transactionDate",fromDate));
		
		List<TransactionDetails> data = transactionDetails.list();
		
		JSONArray obj =new JSONArray();
		for (TransactionDetails dataObject : data) {
			JSONObject object =new JSONObject();
			object.put("id", dataObject.getTransactionId());
			object.put("date", formatDate(dataObject.getTransactionDate()));
			object.put("des", dataObject.getDescription());
			object.put("amount", dataObject.getAmount());
			obj.put(object);
		}
		
		return obj;
		
	}

	@Override
	public double getLastMonthExpense(int catId, int userId) {
		
		Date sysDate =null;
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");  
		   LocalDateTime now = LocalDateTime.now();
		   sysDate = Date.from(now.atZone(ZoneId.systemDefault()).toInstant());
		   
		Calendar cal = Calendar.getInstance();
		cal.setTime(sysDate);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		
		String toDate = year+"/"+month+"/"+31;
		String fromDate = year+"/"+month+"/"+1;
		Date endDate = null;
		Date startDate = null;
		try {
			startDate = new SimpleDateFormat("yyyy/MM/dd").parse(fromDate);
			endDate = new SimpleDateFormat("yyyy/MM/dd").parse(toDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		String query ="SELECT SUM(amount) FROM TransactionDetails WHERE transactionDate>=:startDate AND transactionDate<=:endDate AND transactionType=:transactionType AND userId=:userId AND subcategoryId=:subcategoryId ";
		Query monthExp = getSession().createQuery(query);
		monthExp.setDate("startDate", startDate);
		monthExp.setDate("endDate", endDate);
		monthExp.setInteger("userId", userId);
		monthExp.setString("transactionType", "EXP");
		monthExp.setInteger("subcategoryId", catId);
		double sumAmount =  (double) monthExp.uniqueResult();
		
		return sumAmount;
	}

	@Override
	public void updateFutureGoal(int userId) {
		Date sysDate =null;
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");  
		   LocalDateTime now = LocalDateTime.now();
		   sysDate = Date.from(now.atZone(ZoneId.systemDefault()).toInstant());
		
		Criteria transactionDetails= getSession().createCriteria(TransactionDetails.class);
		transactionDetails.add(Restrictions.eq("transactionType","EXP"));
		transactionDetails.add(Restrictions.eq("userId",userId));
		transactionDetails.add(Restrictions.le("transactionDate",sysDate));
		transactionDetails.add(Restrictions.le("sts",0));
		transactionDetails.add(Restrictions.le("payInfo",0));
		List<TransactionDetails> transDetails =transactionDetails.list();
		
		if(transDetails!=null) {
			for (TransactionDetails dataObject : transDetails) {
				dataObject.setSts(1);
				getSession().update(transDetails);
			}
			
			Criteria futureGl= getSession().createCriteria(FutureGoal.class);
			futureGl.add(Restrictions.eq("userId",userId));
			List<FutureGoal> data =transactionDetails.list();
			
			for (FutureGoal dataObject : data) {
				Date todate = dataObject.getToDate();
				LocalDate start = convertUtilDateToLocalDate(dataObject.getToDate());
				LocalDate end =convertUtilDateToLocalDate(sysDate);
				int noofmonth = monthsBetweenIgnoreDays(end, start);
				dataObject.setFutRental(noofmonth);
				getSession().update(transDetails);
			}
		}
		
		
		
	}
	private static int monthsBetweenIgnoreDays(LocalDate start, LocalDate end) {
	    start = start.withDayOfMonth(1);
	    end = end.withDayOfMonth(1);
	    return Months.monthsBetween(start, end).getMonths();
	}
	public static LocalDate convertUtilDateToLocalDate(Date date) {
        if(date==null) return null;
        DateTime dt = new DateTime(date);
        return dt.toLocalDate();
    }

}
