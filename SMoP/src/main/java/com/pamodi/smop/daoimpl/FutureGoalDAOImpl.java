package com.pamodi.smop.daoimpl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.json.JSONArray;

import com.pamodi.smop.dao.FutureGoalDAO;
import com.pamodi.smop.domain.FutureGoal;
import com.pamodi.smop.domain.TransactionDetails;
import com.pamodi.smop.dto.FutureGoalDTO;

import net.sf.json.JSONObject;
@Transactional
public class FutureGoalDAOImpl implements FutureGoalDAO {
	
	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public FutureGoal createGoal(FutureGoalDTO goalDTO) {
		
		LocalDate start = convertUtilDateToLocalDate(goalDTO.getFromDate());
		LocalDate end =convertUtilDateToLocalDate(goalDTO.getToDate());
		
		int noofmonth = monthsBetweenIgnoreDays(start, end);
		double installment = goalDTO.getAmount()/noofmonth;
		
		FutureGoal goal = new FutureGoal();
		goal.setGlName(goalDTO.getGlName());
		goal.setAmount(goalDTO.getAmount());
		goal.setUserId(goalDTO.getUserId());
		goal.setFromDate(goalDTO.getFromDate());
		goal.setToDate(goalDTO.getToDate());
		goal.setNote(goalDTO.getNote());
		goal.setInstallment(installment);
		goal.setPeriod(noofmonth);

		getSession().save(goal);
		
		Calendar calendar=Calendar.getInstance();
		calendar.setTime(goalDTO.getFromDate());
		
		for(int i = 0 ; i < noofmonth ; i++) {
			calendar.add(Calendar.MONTH, 1);
			TransactionDetails trans = new TransactionDetails();
			trans.setTransactionType("EXP");
			trans.setUserId(goalDTO.getUserId());
			trans.setAmount(installment);
			trans.setDescription(goalDTO.getGlName());
			trans.setTransactionDate(calendar.getTime());
						
			getSession().save(trans);
		}

		return goal;
	}
	
	private static int monthsBetweenIgnoreDays(LocalDate start, LocalDate end) {
	    start = start.withDayOfMonth(1);
	    end = end.withDayOfMonth(1);
	    return Months.monthsBetween(start, end).getMonths();
	}
	
	public static LocalDate convertUtilDateToLocalDate(Date date) {
        if(date==null) return null;
        DateTime dt = new DateTime(date);
        return dt.toLocalDate();
    }

	@Override
	public JSONArray getGoalDetails(int userId) {
		
		Criteria futureGoal= getSession().createCriteria(FutureGoal.class);
		futureGoal.add(Restrictions.eq("userId",userId));
		List<FutureGoal> data = futureGoal.list();
		
		JSONArray obj =new JSONArray();
		for (FutureGoal dataObject : data) {
			JSONObject object =new JSONObject();
			object.put("id", dataObject.getFutureGlId());
			object.put("name", dataObject.getGlName());
			
			int period = dataObject.getPeriod();
			int futureRental = dataObject.getFutRental();
			int paidRental = futureRental - period;
			
			double portion = (100*paidRental)/period;
			
			object.put("amount", portion);
			obj.put(object);
		}
		
		return obj;
	}

}
