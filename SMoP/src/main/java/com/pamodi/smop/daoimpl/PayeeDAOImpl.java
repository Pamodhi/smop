package com.pamodi.smop.daoimpl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;

import com.pamodi.smop.dao.PayeeDAO;
import com.pamodi.smop.domain.Payee;
import com.pamodi.smop.dto.PayeeDTO;

@Transactional
public class PayeeDAOImpl implements PayeeDAO {
	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Payee createPayee(PayeeDTO payee) {
		Payee payeeDetails = new Payee();
		payeeDetails.setUserId(payee.getUserId());
		payeeDetails.setPayeeName(payee.getPayeeName());
		payeeDetails.setAccountNo(payee.getAccountNo());
		payeeDetails.setEmail(payee.getEmail());
		payeeDetails.setPhoneNo(payee.getPhoneNo());
		
		getSession().save(payeeDetails);
		return null;
	}

	public JSONArray getPayee(int userId) {
		Criteria userDetails= getSession().createCriteria(Payee.class);
		userDetails.add(Restrictions.eq("userId",userId));
		List<Payee> data = userDetails.list();
		
		JSONArray obj =new JSONArray();
		for (Payee dataObject : data) {
			JSONObject userObject =new JSONObject();
			userObject.put("id", dataObject.getPayeeId());
			userObject.put("name", dataObject.getPayeeName());
			obj.put(userObject);
		}
		
		return obj;
	}

	public JSONArray getPayeeTransDetails(int id) {
		return null;
	}
}
