package com.pamodi.smop.daoimpl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;

import com.pamodi.smop.dao.FamilySyncDAO;
import com.pamodi.smop.domain.FamilySync;
import com.pamodi.smop.domain.FamilySyncMap;
import com.pamodi.smop.domain.TransactionDetails;
import com.pamodi.smop.dto.FamilySyncDTO;

import net.sf.json.JSONObject;
@Transactional
public class FamilySyncDAOImpl implements FamilySyncDAO {
	
	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public FamilySync createGroup(FamilySyncDTO family) {
		
		 FamilySync familySunc = new FamilySync();
		 familySunc.setDescription(family.getDescription());
		 familySunc.setGroupCode(family.getGroupCode());
		 familySunc.setGroupName(family.getGroupName());
		 familySunc.setUserId(family.getUserId()); 
		 getSession().save(familySunc);
		 
		 FamilySyncMap mapFamily = new FamilySyncMap();
		 mapFamily.setGroupId(familySunc.getGroupId());
		 mapFamily.setUserId(familySunc.getUserId());
		 mapFamily.setMemberType("H");
		 getSession().save(mapFamily);
		 
		return familySunc;
	}

	@Override
	public boolean checkgroupExist(String code, String name, int userId) {
		Criteria family= getSession().createCriteria(FamilySync.class);
		family.add(Restrictions.eq("groupName",name));
		family.add(Restrictions.eq("groupCode",code));
		FamilySync data = (FamilySync) family.uniqueResult();
		
		if(data !=null) {
			FamilySyncMap groupMap = new FamilySyncMap();
			groupMap.setUserId(userId);
			groupMap.setGroupId(data.getGroupId());
			groupMap.setMemberType("M");
			getSession().save(groupMap);
			
			return true;
		}else {
			return false;
		}
	}
	
	public JSONArray getAlltransactions(int userId) {
		Criteria transaction= getSession().createCriteria(TransactionDetails.class);
		transaction.add(Restrictions.eq("userId",userId));
		List<TransactionDetails> data = transaction.list();

		JSONArray obj =new JSONArray();
		for (TransactionDetails dataObject : data) {
			JSONObject object =new JSONObject();
			object.put("id", dataObject.getTransactionId());
			object.put("date", formatDate(dataObject.getTransactionDate()));
			object.put("des", dataObject.getDescription());
			object.put("amount", dataObject.getAmount());
			obj.put(object);
		}
		
		return obj;
	}
	
	public String formatDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String newDate =sdf.format(date);
		
		return newDate;
		
	}

}
