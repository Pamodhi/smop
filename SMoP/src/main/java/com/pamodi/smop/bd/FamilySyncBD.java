package com.pamodi.smop.bd;

import org.json.JSONArray;

import com.pamodi.smop.domain.FamilySync;
import com.pamodi.smop.dto.FamilySyncDTO;

public interface FamilySyncBD {
	public FamilySync createGroup(FamilySyncDTO family) throws Exception;
	public boolean checkgroupExist(String code, String name, int userId) throws Exception;
	public JSONArray getAlltransactions(int userId) throws Exception;
	
}
