package com.pamodi.smop.bd;

import java.util.Date;

import org.json.JSONArray;

public interface ReportBD {
	public JSONArray getBudget(Date toDate, Date fromDate,int userId) throws Exception;
	public JSONArray getAccountTrans(Date toDate, Date fromDate,int userId) throws Exception;
	public JSONArray getExpenses(Date toDate, Date fromDate,int userId) throws Exception;
	public JSONArray getIncome(Date toDate, Date fromDate,int userId) throws Exception;

}
