package com.pamodi.smop.bd;

import java.util.Date;

import org.json.JSONArray;

import com.pamodi.smop.domain.PaymentInfo;
import com.pamodi.smop.domain.TransactionDetails;
import com.pamodi.smop.dto.PaymentInfoDTO;
import com.pamodi.smop.dto.TransactionDTO;

import net.sf.json.JSONObject;

public interface ExpenseBD {
	public TransactionDetails createExpenseDetails(TransactionDTO transactionDTO) throws Exception;
	public void updateExpenseDetails(TransactionDTO transactionDTO) throws Exception;
	public void deleteExpenseDetails(int id) throws Exception;
	public JSONObject getExpenseDetails(int id, int userID) throws Exception;
	public JSONArray getExpenseDetails(int userId) throws Exception;
	public JSONObject getMonthlyExpese(int userId) throws Exception;
	public JSONArray getTransactionDateRange(Date toDate, Date fromDate, int userId) throws Exception;
	public double getLastMonthExpense(int catId, int userId) throws Exception;
	public JSONArray getSubcategory() throws Exception;
	public void updateFutureGoal(int userId) throws Exception;
}
