package com.pamodi.smop.bd;

import org.json.JSONArray;

import com.pamodi.smop.domain.FutureGoal;
import com.pamodi.smop.dto.FutureGoalDTO;

public interface FutureGoalBD {
	public FutureGoal createGoal(FutureGoalDTO goalDTO)throws Exception;
	public JSONArray getGoalDetails(int userId) throws Exception;
}
