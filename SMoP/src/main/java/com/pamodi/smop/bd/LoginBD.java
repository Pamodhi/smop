package com.pamodi.smop.bd;

import com.pamodi.smop.domain.UserLoginDetails;
import com.pamodi.smop.dto.UserLoginDTO;

public interface LoginBD {
	public UserLoginDetails getUserLoginDetail(String userEmail, String password) throws Exception;
	public UserLoginDetails insertLoginDetails(UserLoginDTO userloginDTO) throws Exception;
	public boolean checkUserExist(String email) throws Exception;
	public boolean updateToken(String email , String authenticationToken) throws Exception;
	public int userType(int userId) throws Exception;
	public boolean updatePassword(String password, String email)throws Exception;

}
