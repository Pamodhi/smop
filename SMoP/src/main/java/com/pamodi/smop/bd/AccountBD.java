package com.pamodi.smop.bd;

import org.json.JSONArray;

import com.pamodi.smop.domain.AccountTransaction;
import com.pamodi.smop.domain.Accounts;
import com.pamodi.smop.dto.AccountDTO;
import com.pamodi.smop.dto.TransactionDTO;

import net.sf.json.JSONObject;

public interface AccountBD {
	public Accounts createAccount(AccountDTO accountDTO) throws Exception;
	public JSONArray getAccounts(int userId) throws Exception;
	public JSONObject getAccountBalance(int userId) throws Exception;
	public AccountTransaction createAccountTrans(String type, Accounts account) throws Exception;
	public AccountTransaction createAccountTrans(AccountTransaction account, int userId) throws Exception;
	public void deleteAccountTrans(int tarnsId) throws Exception;
	public void updateAccountTrans(int tarnsId, TransactionDTO transactionDTO) throws Exception;

}
