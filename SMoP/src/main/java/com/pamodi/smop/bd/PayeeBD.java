package com.pamodi.smop.bd;

import org.json.JSONArray;

import com.pamodi.smop.domain.Payee;
import com.pamodi.smop.dto.PayeeDTO;

public interface PayeeBD {
	
	public Payee createPayee(PayeeDTO payee) throws Exception;
	public JSONArray getPayee(int userId) throws Exception;
	public JSONArray getPayeeTransDetails(int id) throws Exception;

}
