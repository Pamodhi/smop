package com.pamodi.smop.bd;

import org.json.JSONArray;

import com.pamodi.smop.domain.UserDetails;
import com.pamodi.smop.dto.UserDetailsDTO;

import net.sf.json.JSONObject;

public interface UserRegistrationBD {
	public Integer insertProfile(UserDetailsDTO userDetailsDTO) throws Exception;
	public JSONArray getProfession() throws Exception;
	public JSONArray getCurrency() throws Exception;
	public boolean checkUserExist(String nic) throws Exception;
	public Integer checkEmailExist(String email) throws Exception;
	public UserDetails verifyCode(int code, String email) throws Exception;
	public boolean verifyUser(int userId) throws Exception;
	public Integer getcode(int userId) throws Exception;
	public JSONObject getUserDetails(int userId) throws Exception;
}
