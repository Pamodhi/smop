package com.pamodi.smop.bd;

import org.json.JSONArray;

public interface AdminBD {

	public String confirmUserRegistration(int userId, String status)throws Exception;
	public JSONArray getUserList() throws Exception;

}
