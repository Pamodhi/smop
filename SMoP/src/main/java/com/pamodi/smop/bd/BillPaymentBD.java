package com.pamodi.smop.bd;


import org.json.JSONArray;

import com.pamodi.smop.domain.PaymentInfo;
import com.pamodi.smop.domain.TransactionDetails;
import com.pamodi.smop.dto.PaymentInfoDTO;
import com.pamodi.smop.dto.TransactionDTO;

import net.sf.json.JSONObject;

public interface BillPaymentBD {
	public JSONArray getSubcategory() throws Exception;
	public JSONObject getMonthlyBill(int userId) throws Exception;
	public PaymentInfo createBill(PaymentInfoDTO payInfo) throws Exception;
	public void deleteBill(int id) throws Exception;
	public JSONArray getbillDetails(int userId) throws Exception;
	public void payBill(int id) throws Exception;

}
