package com.pamodi.smop.bd;

import org.json.JSONArray;

import com.pamodi.smop.domain.TransactionDetails;
import com.pamodi.smop.dto.TransactionDTO;

import net.sf.json.JSONObject;

public interface IncomeBD {
	public TransactionDetails createIncomeDetails(TransactionDTO transactionDTO) throws Exception;
	public void updateIncomeDetails(TransactionDTO transactionDTO) throws Exception;
	public void deleteIncomeDetails(int id) throws Exception;
	public JSONObject getIncomeDetails(int transId,int userId) throws Exception;
	public JSONArray getIncomeDetails(int userId) throws Exception;
	public JSONObject getMonthlyIncome(int userId) throws Exception;

}
