package com.pamodi.smop.util;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.pamodi.smop.bean.UserConfig;

public class SessionUtil {
	private static Logger logger = Logger.getLogger(SessionUtil.class);
	private static final String SESSION_KEY = "LOGIN_KEY";
	private static final String USER_CONFIG_KEY = "USER_CONFIG_KEY";
	private static final String USERLOG_ID_KEY = "USERLOG_ID";
	
	public static boolean isValidSession(HttpServletRequest request) {
		if (request.getSession(false) == null || (request.getSession().getAttribute("LOGIN_KEY") == null)) {
			return false;
		} else {
			return true;
		}
	}
	
	public static void setUserSession(HttpServletRequest request, UserConfig userConfig) {
		HttpSession sessionHTTP = request.getSession(false);
		if (sessionHTTP != null) {
			sessionHTTP.invalidate();
		}
		sessionHTTP = request.getSession(true);
		sessionHTTP.setAttribute(USER_CONFIG_KEY, userConfig);
	}
	
	public static UserConfig getUserSession(HttpServletRequest request) {
		UserConfig session = (UserConfig) request.getSession().getAttribute(USER_CONFIG_KEY);
		logger.info("<HTTP Session ID> " + request.getSession().getId());
		return session;
	}
}
