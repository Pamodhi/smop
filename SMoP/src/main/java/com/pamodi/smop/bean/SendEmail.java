package com.pamodi.smop.bean;

import java.io.IOException;
import java.io.InputStream;

import javax.activation.CommandMap;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class SendEmail {
	
	static String emailToRecipient, emailSubject, emailMessage;
	static final String emailFromRecipient = "smartmoneyplanner.infor@gmail.com";
	
	@Autowired
    private JavaMailSender mailSenderObj;
	
	public boolean sendEmail(String subject, String message, String mailTo, final CommonsMultipartFile attachFileObj) {
		
		emailSubject = subject;
        emailMessage = message;
        emailToRecipient = mailTo ;
 
		mailSenderObj.send(new MimeMessagePreparator() {
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper mimeMsgHelperObj = new MimeMessageHelper(mimeMessage, true, "UTF-8");             
                mimeMsgHelperObj.setTo(emailToRecipient);
                mimeMsgHelperObj.setFrom(emailFromRecipient);               
                mimeMsgHelperObj.setText(emailMessage);
                mimeMsgHelperObj.setSubject(emailSubject);
				
             // Determine If There Is An File Upload. If Yes, Attach It To The Client Email              
                if ((attachFileObj != null) && (attachFileObj.getSize() > 0) && (!attachFileObj.equals(""))) {
                    System.out.println("\nAttachment Name?= " + attachFileObj.getOriginalFilename() + "\n");
                    mimeMsgHelperObj.addAttachment(attachFileObj.getOriginalFilename(), new InputStreamSource() {                   
                        public InputStream getInputStream() throws IOException {
                            return attachFileObj.getInputStream();
                        }
                    });
                } else {
                    System.out.println("\nNo Attachment Is Selected By The User. Sending Text Email!\n");
                }
			}
        });
        System.out.println("\nMessage Send Successfully.... Hurrey!\n");
		return false;
		
	}
	
	public String sendEmail(String email, int verifyode) {
		
		int code=0;
		if(verifyode !=0) {
			code=verifyode;
		}else {
			code = GenarateCode.userVerificationCode();
		}
		String subject="Verify Account";
	    String message="OTP =" + code;
	    String mailTo=email;
	    
	    SimpleMailMessage emailSend = new SimpleMailMessage();
	    emailSend.setTo(mailTo);
	    emailSend.setSubject(subject);
	    emailSend.setText(message);
         
        //sends the e-mail
	    mailSenderObj.send(emailSend);
		return mailTo;
		
	}

}
