package com.pamodi.smop.bean;

public class UserConfig {
	private int userId;
	private int userName;
	private int logType;
	private int userAccessType;
	private int token;
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getUserName() {
		return userName;
	}
	public void setUserName(int userName) {
		this.userName = userName;
	}
	public int getLogType() {
		return logType;
	}
	public void setLogType(int logType) {
		this.logType = logType;
	}
	public int getUserAccessType() {
		return userAccessType;
	}
	public void setUserAccessType(int userAccessType) {
		this.userAccessType = userAccessType;
	}
	
	
}
