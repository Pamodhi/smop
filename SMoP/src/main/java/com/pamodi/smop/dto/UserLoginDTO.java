package com.pamodi.smop.dto;

public class UserLoginDTO {
	
	private int loginId;
	private String email;
	private String userName;
	private String userPassword;
	private int userAccessTypeId;
	private int isRegistered;
	private long token;
	private int userId;
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getLoginId() {
		return loginId;
	}
	public void setLoginId(int loginId) {
		this.loginId = loginId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public int getUserAccessTypeId() {
		return userAccessTypeId;
	}
	public void setUserAccessTypeId(int userAccessTypeId) {
		this.userAccessTypeId = userAccessTypeId;
	}
	public int getIsRegistered() {
		return isRegistered;
	}
	public void setIsRegistered(int isRegistered) {
		this.isRegistered = isRegistered;
	}
	public long getToken() {
		return token;
	}
	public void setToken(long token) {
		this.token = token;
	}
	
	
	
}
