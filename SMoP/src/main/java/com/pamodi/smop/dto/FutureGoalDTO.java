package com.pamodi.smop.dto;

import java.util.Date;

public class FutureGoalDTO {
	private int futureGlId;
	private int userId;
	private String glName;
	private int amount;
	private Date toDate;
	private Date fromDate;
	private String note;
	public int getFutureGlId() {
		return futureGlId;
	}
	public void setFutureGlId(int futureGlId) {
		this.futureGlId = futureGlId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getGlName() {
		return glName;
	}
	public void setGlName(String glName) {
		this.glName = glName;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	

}
