package com.pamodi.smop.controller;

import java.sql.SQLException;

import org.json.JSONArray;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pamodi.smop.bd.AccountBD;
import com.pamodi.smop.bd.BillPaymentBD;
import com.pamodi.smop.bd.ExpenseBD;
import com.pamodi.smop.bd.IncomeBD;
import com.pamodi.smop.bd.LoginBD;
import com.pamodi.smop.bd.TokenBD;
import com.pamodi.smop.bd.UserRegistrationBD;

import net.sf.json.JSONObject;

@CrossOrigin("*")
@RestController
@RequestMapping(path = "/DashBoadManager")
public class DashBoardController {
	
	private LoginBD loginBD;
	private TokenBD tokenBD;
	private ExpenseBD expenseBD;
	private BillPaymentBD billPaymentBD;
	private IncomeBD incomeBD;
	private UserRegistrationBD userBD;
	private AccountBD accountBD;
	public LoginBD getLoginBD() {
		return loginBD;
	}
	public void setLoginBD(LoginBD loginBD) {
		this.loginBD = loginBD;
	}
	public TokenBD getTokenBD() {
		return tokenBD;
	}
	public void setTokenBD(TokenBD tokenBD) {
		this.tokenBD = tokenBD;
	}
	public ExpenseBD getExpenseBD() {
		return expenseBD;
	}
	public void setExpenseBD(ExpenseBD expenseBD) {
		this.expenseBD = expenseBD;
	}
	public BillPaymentBD getBillPaymentBD() {
		return billPaymentBD;
	}
	public void setBillPaymentBD(BillPaymentBD billPaymentBD) {
		this.billPaymentBD = billPaymentBD;
	}
	public IncomeBD getIncomeBD() {
		return incomeBD;
	}
	public void setIncomeBD(IncomeBD incomeBD) {
		this.incomeBD = incomeBD;
	}
	
	public UserRegistrationBD getUserBD() {
		return userBD;
	}
	public void setUserBD(UserRegistrationBD userBD) {
		this.userBD = userBD;
	}
	public AccountBD getAccountBD() {
		return accountBD;
	}
	public void setAccountBD(AccountBD accountBD) {
		this.accountBD = accountBD;
	}
	@RequestMapping(path="/getUserType", method = RequestMethod.GET)
	@ResponseBody
	public String getUserType(@RequestParam int userId) throws Exception {
	
		 int userType = getLoginBD().userType(userId);
		 JSONObject object = new JSONObject();
		 object.put("userType",userType);
		
		return object.toString();
		
	}
	
	@RequestMapping(path="/getCurrentMonthExp", method = RequestMethod.GET)
	@ResponseBody
	public String getCurrentMonthExp(@RequestHeader(value="userId") int userId, @RequestHeader(value="token") String token) throws Exception {
		JSONObject responseobject = new JSONObject();
		try {
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				responseobject = getExpenseBD().getMonthlyExpese(userId);
				
			}
		}catch(SQLException e) {
        	e.printStackTrace();
        }
		
		return responseobject.toString();
		
	}
	@RequestMapping(path="/getCurrentMonthInc", method = RequestMethod.GET)
	@ResponseBody
	public String getCurrentMonthInc(@RequestHeader(value="userId") int userId, @RequestHeader(value="token") String token) throws Exception {
		JSONObject responseobject = new JSONObject();
		try {
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				responseobject = getIncomeBD().getMonthlyIncome(userId);
				
			}
		}catch(SQLException e) {
        	e.printStackTrace();
        }
		
		return responseobject.toString();
		
	}
	
	@RequestMapping(path="/getCurrentMonthBill", method = RequestMethod.GET)
	@ResponseBody
	public String getCurrentMonthBill(@RequestHeader(value="userId") int userId, @RequestHeader(value="token") String token) throws Exception {
		JSONObject responseobject = new JSONObject();
		try {
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				responseobject = getBillPaymentBD().getMonthlyBill(userId);
				
			}
		}catch(SQLException e) {
        	e.printStackTrace();
        }
		
		return responseobject.toString();
		
	}
	
	@RequestMapping(path="/getAccountBalance", method = RequestMethod.GET)
	@ResponseBody
	public String getAccountBalance(@RequestHeader(value="userId") int userId, @RequestHeader(value="token") String token) throws Exception {
		JSONObject responseobject = new JSONObject();
		try {
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				responseobject = getAccountBD().getAccountBalance(userId);
			}
		}catch(SQLException e) {
        	e.printStackTrace();
        }
		
		return responseobject.toString();
		
	}
	
	@RequestMapping(path="/getUserDetails", method = RequestMethod.GET)
	@ResponseBody
	public String getUserDetails(@RequestHeader(value="userId") int userId, @RequestHeader(value="token") String token) throws Exception {
		JSONObject responseobject = new JSONObject();
		try {
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				responseobject = getUserBD().getUserDetails(userId);
			}
		}catch(SQLException e) {
        	e.printStackTrace();
        }
		
		return responseobject.toString();
		
	}
	
	@RequestMapping(path="/checkTransactions", method = RequestMethod.GET)
	@ResponseBody
	public String checkTransactions(@RequestHeader(value="userId") int userId, @RequestHeader(value="token") String token) throws Exception {
		JSONObject responseobject = new JSONObject();
		try {
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				getExpenseBD().updateFutureGoal(userId);
			}
		}catch(SQLException e) {
        	e.printStackTrace();
        }
		
		return responseobject.toString();
		
	}

}
