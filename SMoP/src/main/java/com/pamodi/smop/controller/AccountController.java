package com.pamodi.smop.controller;

import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.json.JSONArray;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pamodi.smop.bd.AccountBD;
import com.pamodi.smop.bd.TokenBD;
import com.pamodi.smop.domain.Accounts;
import com.pamodi.smop.dto.AccountDTO;

import net.sf.json.JSONObject;

@CrossOrigin("*")
@RestController
@RequestMapping(path = "/AccountManager")
public class AccountController {
	
	private TokenBD tokenBD;
	private AccountBD accountBD;
	
	public TokenBD getTokenBD() {
		return tokenBD;
	}
	public void setTokenBD(TokenBD tokenBD) {
		this.tokenBD = tokenBD;
	}
	public AccountBD getAccountBD() {
		return accountBD;
	}
	public void setAccountBD(AccountBD accountBD) {
		this.accountBD = accountBD;
	}
	
	@RequestMapping(path="/createAccount", method = RequestMethod.POST)
	@ResponseBody
	public String createIncome(@Validated @RequestBody String reqData, 
			@RequestHeader(value="userId") int userId, @RequestHeader(value="token") String token ) throws Exception {
		
		JSONObject responseobject = new JSONObject();
		JSONObject reqObject = (JSONObject) JSONObject.fromObject(reqData);
		
		int account = 0;
		String name = null; 
		int currency = 0;
		int balance = 0;
		
		try {
			if(!reqObject.get("account_name").equals("")) {
				name = (String) reqObject.get("account_name");
			}
			if(!reqObject.get("account_no").equals("")) {
				account =Integer.parseInt( (String) reqObject.get("account_no"));
			}
			if(!reqObject.get("account_balance").equals("")) {
				balance =  Integer.parseInt( (String) reqObject.get("account_balance"));
			}
			if(!reqObject.get("currency").equals("")) {
				currency =  Integer.parseInt( (String) reqObject.get("currency"));
			}
			
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				AccountDTO accountDTO = new AccountDTO();
				accountDTO.setAccountName(name);
				accountDTO.setAccountNo(account);
				accountDTO.setAccountBalance(balance);
				accountDTO.setCurrencyId(currency);
				accountDTO.setUserId(userId);
				
				Accounts accountDetails = getAccountBD().createAccount(accountDTO);
				getAccountBD().createAccountTrans("CR", accountDetails);
				
				responseobject.put("msg", "Success");
			}else {
				responseobject.put("msg", "invalidUser");
			}
			
		}catch(Exception e) {
			responseobject.put("msg", e);
		}
		return responseobject.toString();
	}
	
	@RequestMapping(path="/getAccount", method = RequestMethod.GET)
	@ResponseBody
	public String getAccount(@RequestHeader(value="token") String token, @RequestHeader(value="userId") int userId) throws Exception {
		JSONArray array =new JSONArray();
		try {
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				array = getAccountBD().getAccounts(userId);
			}
		}catch(SQLException e) {
        	e.printStackTrace();
        }
		
		return array.toString();
	}
	
	@RequestMapping(path="/getAccountBalance", method = RequestMethod.GET)
	@ResponseBody
	public String getAccountBalance(@RequestHeader(value="token") String token, @RequestHeader(value="userId") int userId) throws Exception {
		JSONObject object =new JSONObject();
		try {
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				object = getAccountBD().getAccountBalance(userId);
			}
		}catch(SQLException e) {
        	e.printStackTrace();
        }
		
		return object.toString();
	}
	
}
