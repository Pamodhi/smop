package com.pamodi.smop.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pamodi.smop.bd.AdminBD;
import com.pamodi.smop.bd.TokenBD;
import com.pamodi.smop.domain.UserLoginDetails;

@CrossOrigin("*")
@RestController
@RequestMapping(path = "/AdminManager")
public class AdminController {
	private AdminBD adminBD;
	private TokenBD tokenBD;

	public AdminBD getAdminBD() {
		return adminBD;
	}
	public void setAdminBD(AdminBD adminBD) {
		this.adminBD = adminBD;
	}
	public TokenBD getTokenBD() {
		return tokenBD;
	}
	public void setTokenBD(TokenBD tokenBD) {
		this.tokenBD = tokenBD;
	}
	
	@RequestMapping(path="/getApprove", method = RequestMethod.GET)
	@ResponseBody
	public String getApprove(@RequestParam(value = "id") int id,@RequestHeader(value="token") String token, @RequestHeader(value="userId") int userId) throws Exception{
		JSONArray array =new JSONArray();
		JSONObject object = new JSONObject();
		String status="A";
		try {
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				getAdminBD().confirmUserRegistration(id,status);
				object.put("msg", "Success");
			}
			
		}catch(SQLException e) {
        	e.printStackTrace();
        }
		
		return object.toString();
	}
	
	
	@RequestMapping(path="/getRegject", method = RequestMethod.GET)
	@ResponseBody
	public String getReject(@RequestParam(value = "id") int id,@RequestHeader(value="token") String token, @RequestHeader(value="userId") int userId) throws Exception{
		JSONArray array =new JSONArray();
		JSONObject object = new JSONObject();
		String status="R";
		try {
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				getAdminBD().confirmUserRegistration(id,status);
				object.put("msg", "Success");
			}
		}catch(SQLException e) {
        	e.printStackTrace();
        }
		
		return object.toString();
	}
	
	
	@RequestMapping(path="/getUsers", method = RequestMethod.GET)
	@ResponseBody
	public String getUserDetails(@RequestHeader(value="userId") int userId, @RequestHeader(value="token") String token) throws Exception{
		JSONArray array =new JSONArray();
		try {
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				array = getAdminBD().getUserList();
			}
		
		}catch(SQLException e) {
        	e.printStackTrace();
        }
		
		return array.toString();
				
		
	}

}
