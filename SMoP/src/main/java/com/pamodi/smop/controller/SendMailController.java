package com.pamodi.smop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping(path = "/SendEmail")
public class SendMailController {
	
	@Autowired
    private JavaMailSender mailSender;
	
	@RequestMapping(path="/sendEmail", method = RequestMethod.GET)
	@ResponseBody
	public String updatePassword() {
		int code=1234;
	    String subject="Verify Account";
	    String message="OTP =" + code;
	    String mailTo="shashipiyadigama94@gmail.com";
	    
	    SimpleMailMessage emailSend = new SimpleMailMessage();
	    emailSend.setTo(mailTo);
	    emailSend.setSubject(subject);
	    emailSend.setText(message);
	     
	    // sends the e-mail
	    mailSender.send(emailSend);
		return mailTo;
	}
	
	

}
