package com.pamodi.smop.controller;

import java.sql.SQLException;
import java.util.LinkedList;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.mysql.cj.xdevapi.JsonArray;
import com.pamodi.smop.domain.UserDetails;
import com.pamodi.smop.domain.UserLoginDetails;
import com.pamodi.smop.dto.UserLoginDTO;
import com.pamodi.smop.bd.LoginBD;
import com.pamodi.smop.bean.GenarateCode;
import com.pamodi.smop.bean.GenerateToken;
import com.pamodi.smop.bean.SendEmail;
import com.pamodi.smop.bean.TrippleDes;

import net.sf.json.JSONObject;

@CrossOrigin("*")
@Controller
@RequestMapping(path = "/LoginManager")
public class LoginController {
	
	private LoginBD loginBD;
	public LoginBD getLoginBD() {
		return loginBD;
	}
	public void setLoginBD(LoginBD loginBD) {
		this.loginBD = loginBD;
	}
	
	GenerateToken generateToken;
	TrippleDes passwordEncript;
	
	@RequestMapping(path="/login", method = RequestMethod.POST)
	@ResponseBody
	public String login(@Validated @RequestBody String reqData) throws Exception {
		UserLoginDetails userlog = new UserLoginDetails();
		JSONObject responseobject = new JSONObject();
		JSONObject reqObject = (JSONObject) JSONObject.fromObject(reqData);
		String userEmail = null;
		String password =null;
		
		try {
			if(!reqObject.get("userEmail").equals("")) {
				userEmail = (String) reqObject.get("userEmail");
			}
			if(!reqObject.get("password").equals("")) {
				password = (String) reqObject.get("password");
			}
			
			//String passEncrypt = passwordEncript.encrypt(password);
			//userlog = getLoginBD().getUserLoginDetail(userEmail, passEncrypt);
			userlog = getLoginBD().getUserLoginDetail(userEmail, password);
			if(userlog != null) {
				if(userlog.getIsVerify() == 0) {
					responseobject.put("notverify", "Please verify your account");
					responseobject.put("email", userlog.getEmail());
				}else {
					String token = GenerateToken.createJWT(userlog.getEmail(), "JWT Token",userlog.getUserName(), 43200000, userlog.getUserId());
					getLoginBD().updateToken(userEmail, token);
					responseobject.put("response", "Success");
					responseobject.put("token", token);
					responseobject.put("accessType", userlog.getUserAccessTypeId());
					responseobject.put("userId", userlog.getUserId());
				}
			}else {
				boolean isEmailExsit = getLoginBD().checkUserExist(userEmail);
				if(isEmailExsit) {
					responseobject.put("error", "Sorry User Password is Wrong. Plaese Enter valid Password");
				}else {
					responseobject.put("error", "Sorry User Email is Wrong. Plaese Enter valid Login Details");
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return responseobject.toString();
		
	}
	
	@RequestMapping(path="/forgetPass", method = RequestMethod.POST)
	@ResponseBody
	public String forgetPassword(@Validated @RequestParam String email) throws Exception{
		JSONObject responseobject=new JSONObject(); 
		try {
			boolean userExist  = getLoginBD().checkUserExist(email);
			if(userExist) {
				
			}else {
				responseobject.put("error","email not found");
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return responseobject.toString();
	}
	
	@RequestMapping(path="/updatePassword", method = RequestMethod.POST)
	@ResponseBody
	public String updatePassword(@Validated @RequestParam String password, @RequestHeader(value="userId") String email )throws Exception{
		JSONObject responseobject=new JSONObject(); 
		boolean ischancePass = getLoginBD().updatePassword(password, email) ;
		if(ischancePass) {
			responseobject.put("msg","Success");
		}
		return responseobject.toString();
		
	}
}
