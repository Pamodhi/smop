package com.pamodi.smop.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pamodi.smop.bd.BillPaymentBD;
import com.pamodi.smop.bd.TokenBD;
import com.pamodi.smop.domain.PaymentInfo;
import com.pamodi.smop.dto.PaymentInfoDTO;
import com.pamodi.smop.dto.TransactionDTO;

import net.sf.json.JSONObject;

@CrossOrigin("*")
@RestController
@RequestMapping(path = "/BillPayment")
public class BillPaymentController {
	
	private BillPaymentBD billPaymentBD;
	private TokenBD tokenBD;
	
	public BillPaymentBD getBillPaymentBD() {
		return billPaymentBD;
	}
	public void setBillPaymentBD(BillPaymentBD billPaymentBD) {
		this.billPaymentBD = billPaymentBD;
	}
	public TokenBD getTokenBD() {
		return tokenBD;
	}
	public void setTokenBD(TokenBD tokenBD) {
		this.tokenBD = tokenBD;
	}
	
	@RequestMapping(path="/createBill", method = RequestMethod.POST)
	@ResponseBody
	public String createBill(@RequestHeader(value="token") String token, @RequestHeader(value="userId") int userId,
			@RequestBody String reqData) throws Exception {
		
		JSONObject responseobject = new JSONObject();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		JSONObject reqObject = (JSONObject) JSONObject.fromObject(reqData);
		
		int amount = 0;
		String description = null;
		Date dueDate = null;
		int accountId=0;
		int payeeId=0;
		int categoryId =0;
		try {
			if(!reqObject.get("expen_amount").equals("")) {
				amount =Integer.parseInt( (String) reqObject.get("expen_amount"));
			}
			if(!reqObject.get("expen_note").equals("")) {
				description = (String) reqObject.get("expen_note");
			}
			if(!reqObject.get("expen_date").equals("")) {
				dueDate = sdf.parse((String) reqObject.get("expen_date"));
			}
			if(!reqObject.get("expen_payee").equals("")) {
				payeeId =Integer.parseInt( (String) reqObject.get("expen_payee"));
			}
			if(!reqObject.get("expen_category").equals("")) {
				categoryId =Integer.parseInt( (String) reqObject.get("expen_category"));
			}
			if(!reqObject.get("expen_account").equals("")) {
				accountId =Integer.parseInt( (String) reqObject.get("expen_account"));
			}
			
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				PaymentInfoDTO payInfo = new PaymentInfoDTO();
				payInfo.setUserId(userId);
				payInfo.setAmount(amount);
				payInfo.setDescription(description);
				payInfo.setDueDate(dueDate);
				payInfo.setSubcategoryId(categoryId);
				payInfo.setAccountId(accountId);
				payInfo.setPayeeId(payeeId);
				
				PaymentInfo payDetails = getBillPaymentBD().createBill(payInfo);
				
			}
			
		} catch (Exception e) {
			responseobject.put("msg", e);
		}
		return responseobject.toString();
		
	}
	
	@RequestMapping(path="/deleteBill/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public String deleteBill(@PathVariable(value = "id") int id,
			@RequestHeader(value="token") String token, @RequestHeader(value="userId") int userId) throws Exception {
		
		boolean isValid = getTokenBD().isTokenExist(token, userId);
		if(isValid) {
			getBillPaymentBD().deleteBill(id);
		}	
		return null;
		
	}
	
	@RequestMapping(path="/getBillDetails", method = RequestMethod.GET)
	@ResponseBody
	public String getBillDetails(@RequestHeader(value="token") String token, @RequestHeader(value="userId") int userId) throws Exception {
		JSONArray array =new JSONArray();
		
		boolean isValid = getTokenBD().isTokenExist(token, userId);
		if(isValid) {
			array=getBillPaymentBD().getbillDetails(userId);
		}
		
		return array.toString();
		
	}
	
	@RequestMapping(path="/payBill", method = RequestMethod.PUT)
	@ResponseBody
	public String payBill(@RequestHeader(value="token") String token, @RequestHeader(value="userId") int userId, 
			@RequestParam int id) throws Exception {
		JSONObject object =new JSONObject();
		
		boolean isValid = getTokenBD().isTokenExist(token, userId);
		if(isValid) {
			getBillPaymentBD().payBill(id);
		}
		
		return object.toString();
		
	}

}
