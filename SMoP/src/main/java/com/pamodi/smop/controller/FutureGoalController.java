package com.pamodi.smop.controller;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pamodi.smop.bd.FutureGoalBD;
import com.pamodi.smop.bd.TokenBD;
import com.pamodi.smop.bean.GenerateToken;
import com.pamodi.smop.dto.FutureGoalDTO;

import io.jsonwebtoken.Claims;
import net.sf.json.JSONObject;

@CrossOrigin("*")
@RestController
@RequestMapping(path = "/FutureGoal")
public class FutureGoalController {
	
	private FutureGoalBD futureGoalBD;
	private TokenBD tokenBD;
	
	public FutureGoalBD getFutureGoalBD() {
		return futureGoalBD;
	}
	public void setFutureGoalBD(FutureGoalBD futureGoalBD) {
		this.futureGoalBD = futureGoalBD;
	}
	public TokenBD getTokenBD() {
		return tokenBD;
	}
	public void setTokenBD(TokenBD tokenBD) {
		this.tokenBD = tokenBD;
	}
	
	@RequestMapping(path="/createGoal", method = RequestMethod.POST)
	@ResponseBody
	public String createGoal(@Validated @RequestBody String reqData,
			@RequestHeader(value="userId") int userId, @RequestHeader(value="token") String token) throws Exception {
		JSONObject responseobject = new JSONObject();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		JSONObject reqObject = (JSONObject) JSONObject.fromObject(reqData);
		
		String name =null;
		int amount=0;
		Date toDate=null;
		Date fromDate=null;
		String note =null;
		
		try {
			if(!reqObject.get("goal_name").equals("")) {
				name = (String) reqObject.get("goal_name");
			}
			if(!reqObject.get("goal_amount").equals("")) {
				amount =Integer.parseInt( (String) reqObject.get("goal_amount"));
			}
			if(!reqObject.get("goal_toDate").equals("")) {
				toDate = sdf.parse((String) reqObject.get("goal_toDate"));
			}
			if(!reqObject.get("goal_fromDate").equals("")) {
				fromDate = sdf.parse((String) reqObject.get("goal_fromDate"));
			}
			if(!reqObject.get("goal_note").equals("")) {
				note = (String) reqObject.get("goal_note");
			}
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				FutureGoalDTO goalDTO = new FutureGoalDTO();
				goalDTO.setAmount(amount);
				goalDTO.setGlName(name);
				goalDTO.setUserId(1);
				goalDTO.setFromDate(fromDate);
				goalDTO.setToDate(toDate);
				goalDTO.setNote(note);
				
				getFutureGoalBD().createGoal(goalDTO);
				responseobject.put("msg", "Success");
			}
		}catch(Exception e) {
			responseobject.put("msg", e+"Error");
		}
		return responseobject.toString();
	}
	
	@RequestMapping(path="/getGoals", method = RequestMethod.GET)
	@ResponseBody
	public String getGoalDetails(@RequestHeader(value="userId") int userId, @RequestHeader(value="token") String token) throws Exception {
		
		JSONArray array =new JSONArray();
		try {
			Claims claim = GenerateToken.decodeJWT(token);
			int user =Integer.parseInt( claim.getId());
			boolean isValid = getTokenBD().isTokenExist(token, user);
			if(isValid) {
				array = getFutureGoalBD().getGoalDetails(user);
			}
		}catch(SQLException e) {
        	e.printStackTrace();
        }
		
		return array.toString();
		
	}
}
