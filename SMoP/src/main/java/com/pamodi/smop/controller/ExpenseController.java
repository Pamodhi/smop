package com.pamodi.smop.controller;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pamodi.smop.bd.AccountBD;
import com.pamodi.smop.bd.ExpenseBD;
import com.pamodi.smop.bd.PayeeBD;
import com.pamodi.smop.bd.TokenBD;
import com.pamodi.smop.domain.AccountTransaction;
import com.pamodi.smop.domain.PaymentInfo;
import com.pamodi.smop.domain.TransactionDetails;
import com.pamodi.smop.dto.PaymentInfoDTO;
import com.pamodi.smop.dto.TransactionDTO;

import net.sf.json.JSONObject;

@CrossOrigin("*")
@RestController
@RequestMapping(path = "/ExpenseManager")
public class ExpenseController {
	private ExpenseBD expenseBD;
	private TokenBD tokenBD;
	private AccountBD accountBD;
	
	public ExpenseBD getExpenseBD() {
		return expenseBD;
	}
	public void setExpenseBD(ExpenseBD expenseBD) {
		this.expenseBD = expenseBD;
	}
	public TokenBD getTokenBD() {
		return tokenBD;
	}
	public void setTokenBD(TokenBD tokenBD) {
		this.tokenBD = tokenBD;
	}

	public AccountBD getAccountBD() {
		return accountBD;
	}
	public void setAccountBD(AccountBD accountBD) {
		this.accountBD = accountBD;
	}
	@RequestMapping(path="/createExpense", method = RequestMethod.POST)
	@ResponseBody
	public String createExpense(@Validated @RequestBody String reqData,
			@RequestHeader(value="userId") int userId, @RequestHeader(value="token") String token) throws Exception {
		JSONObject responseobject = new JSONObject();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		JSONObject reqObject = (JSONObject) JSONObject.fromObject(reqData);
		String a = reqObject.toString();
		
		int amount = 0;
		String description = null;
		Date transeDate = null;
		int accountId=0;
		int payeeId=0;
		int categoryId =0;
		try {
			if(!reqObject.get("expen_amount").equals("")) {
				amount =Integer.parseInt( (String) reqObject.get("expen_amount"));
			}
			if(!reqObject.get("expen_note").equals("")) {
				description = (String) reqObject.get("expen_note");
			}
			if(!reqObject.get("expen_date").equals("")) {
				transeDate = sdf.parse((String) reqObject.get("expen_date"));
			}
			if(!reqObject.get("expen_payee").equals("") && !reqObject.get("expen_payee").equals(null)) {
				payeeId =Integer.parseInt( (String) reqObject.get("expen_payee"));
			}
			if(!reqObject.get("expen_category").equals("")) {
				categoryId =Integer.parseInt( (String) reqObject.get("expen_category"));
			}
			if(!reqObject.get("expen_account").equals("") && !reqObject.get("expen_payee").equals(null)) {
				accountId =Integer.parseInt( (String) reqObject.get("expen_account"));
			}
			
			TransactionDTO transactionDTO = new TransactionDTO();
			transactionDTO.setUserId(userId);
			transactionDTO.setAmount(amount);
			transactionDTO.setDescription(description);
			transactionDTO.setTransactionType("EXP");
			transactionDTO.setTransactionDate(transeDate);
			transactionDTO.setSubcategoryId(categoryId);
			transactionDTO.setPayeeId(payeeId);
			transactionDTO.setAccountId(accountId);
			
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				getExpenseBD().createExpenseDetails(transactionDTO);
				AccountTransaction account = new AccountTransaction();
				account.setAmount(amount);
				account.setAccountId(accountId);
				account.setTransType("DR");
				account.setTransDate(transeDate);
				if(accountId >0) {
					getAccountBD().createAccountTrans(account, userId);
				}
				
				
				responseobject.put("msg", "Success");
			}else {
				responseobject.put("msg", "invalidUser");
			}
			
		}catch(Exception e) {
			responseobject.put("msg", e+"Error");
		}
		return responseobject.toString();
	}
	
	@RequestMapping(path="/updateExpense/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public String updateExpense(@PathVariable(value = "id") int id, @Validated @RequestBody String reqData,
			@RequestHeader(value="userId") int userId, @RequestHeader(value="token") String token) throws Exception {
		
		JSONObject responseobject = new JSONObject();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		JSONObject reqObject = (JSONObject) JSONObject.fromObject(reqData);
		
		int amount = 0;
		String description = null;
		Date transeDate = null;
		int accountId=0;
		int payeeId=0;
		int categoryId =0;
		try {
			if(!reqObject.get("expen_amount").equals("")) {
				amount =Integer.parseInt( (String) reqObject.get("expen_amount"));
			}
			if(!reqObject.get("expen_note").equals("")) {
				description = (String) reqObject.get("expen_note");
			}
			if(!reqObject.get("expen_date").equals("")) {
				transeDate = sdf.parse((String) reqObject.get("expen_date"));
			}
			if(!reqObject.get("expen_payee").equals("")) {
				payeeId =Integer.parseInt( (String) reqObject.get("expen_payee"));
			}
			if(!reqObject.get("expen_category").equals("")) {
				categoryId =Integer.parseInt( (String) reqObject.get("expen_category"));
			}
			if(!reqObject.get("expen_account").equals("")) {
				accountId =Integer.parseInt( (String) reqObject.get("expen_account"));
			}
			
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				TransactionDTO transactionDTO = new TransactionDTO();
				transactionDTO.setUserId(userId);
				transactionDTO.setAmount(amount);
				transactionDTO.setDescription(description);
				transactionDTO.setTransactionType("EXP");
				transactionDTO.setTransactionDate(transeDate);
				transactionDTO.setSubcategoryId(categoryId);
				transactionDTO.setTransactionId(id);
				if(accountId >0) {
					getAccountBD().updateAccountTrans(id, transactionDTO);
				}
				getExpenseBD().updateExpenseDetails(transactionDTO);
				
				responseobject.put("msg", "Success");
			}
			
		}catch(Exception e) {
			responseobject.put("msg", e);
		}
		
		return responseobject.toString();
		
	}
	
	@RequestMapping(path="/deleteExpense/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public String deleteExpense(@PathVariable(value = "id") int id,
			@RequestHeader(value="userId") int userId, @RequestHeader(value="token") String token) throws Exception {
		
		boolean isValid = getTokenBD().isTokenExist(token, userId);
		if(isValid) {
			getAccountBD().deleteAccountTrans(id);
			getExpenseBD().deleteExpenseDetails(id);
			
		}
		return null;
	}
	
	@RequestMapping(path="/getAllExpenseDetails", method = RequestMethod.GET)
	@ResponseBody
	public String getExpenseDetails(@RequestHeader(value="userId") int userId, @RequestHeader(value="token") String token) throws Exception {
		JSONArray array =new JSONArray();
		try {
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				array = getExpenseBD().getExpenseDetails(userId);
				
			}
		}catch(SQLException e) {
			
        }
		
		return array.toString();
	}
	
	@RequestMapping(path="/getCategoryDetails", method = RequestMethod.GET)
	@ResponseBody
	public String getCategoryDetails(@RequestHeader(value="userId") int userId, @RequestHeader(value="token") String token) throws Exception {
		JSONArray array =new JSONArray();
		try {
			
			array = getExpenseBD().getSubcategory();
		}catch(SQLException e) {
        	e.printStackTrace();
        }
		
		return array.toString();
	}
	
	@RequestMapping(path="/getExpenseById", method = RequestMethod.GET)
	@ResponseBody
	public String getExpenseById(@RequestHeader(value="token") String token, @RequestHeader(value="userId") int userId, 
			@RequestParam int id) throws Exception {
		JSONObject object =new JSONObject();
		
		boolean isValid = getTokenBD().isTokenExist(token, userId);
		if(isValid) {
			object = getExpenseBD().getExpenseDetails(id, userId);
			
		}
		
		return object.toString();
		
	}
	
	@RequestMapping(path="/getTransactionDateRange", method = RequestMethod.GET)
	@ResponseBody
	public String getTransactiondataRange(@RequestHeader(value="token") String token, @RequestHeader(value="userId") int userId, 
			@RequestBody String reqData) throws Exception {
		
		JSONArray array =new JSONArray();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		JSONObject reqObject = (JSONObject) JSONObject.fromObject(reqData);
		
		Date toDate= null;
		Date fromDate =null;
		
		if(!reqObject.get("to_date").equals("")) {
			toDate = sdf.parse((String) reqObject.get("to_date"));
		}
		
		if(!reqObject.get("from_date").equals("")) {
			fromDate = sdf.parse((String) reqObject.get("from_date"));
		}
		
		boolean isValid = getTokenBD().isTokenExist(token, userId);
		if(isValid) {
			array =getExpenseBD().getTransactionDateRange(toDate, fromDate, userId);
		}
		
		return array.toString();
		
	}
	
	@RequestMapping(path="/compireWithLastMonth", method = RequestMethod.GET)
	@ResponseBody
	public String checkExpenses(@RequestHeader(value="token") String token, @RequestHeader(value="userId") int userId, 
			@RequestParam(name = "catId") int catoId, @RequestParam(name = "amount") double amount) throws Exception {
		JSONObject responseobject = new JSONObject();
		
		try {
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				double sumAmount = getExpenseBD().getLastMonthExpense(catoId, userId);
				if(sumAmount > amount) {
					responseobject.put("response", false);
				}else {
					double balance = amount-sumAmount;
					responseobject.put("response", true);
					responseobject.put("balance", balance);
				}
			}
		} catch (Exception e) {
			responseobject.put("msg", e);
		}
		
		return responseobject.toString();
		
	}

}
