package com.pamodi.smop.controller;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pamodi.smop.bd.AccountBD;
import com.pamodi.smop.bd.IncomeBD;
import com.pamodi.smop.bd.TokenBD;
import com.pamodi.smop.bean.GenerateToken;
import com.pamodi.smop.domain.AccountTransaction;
import com.pamodi.smop.domain.Accounts;
import com.pamodi.smop.domain.TransactionDetails;
import com.pamodi.smop.dto.TransactionDTO;

import io.jsonwebtoken.Claims;
import net.sf.json.JSONObject;

@CrossOrigin("*")
@RestController
@RequestMapping(path = "/IncomeManager")
public class IncomeController {
	
	private IncomeBD incomeBD;
	private TokenBD tokenBD;
	private AccountBD accountBD;
	public IncomeBD getIncomeBD() {
		return incomeBD;
	}

	public void setIncomeBD(IncomeBD incomeBD) {
		this.incomeBD = incomeBD;
	}
	
	public TokenBD getTokenBD() {
		return tokenBD;
	}

	public void setTokenBD(TokenBD tokenBD) {
		this.tokenBD = tokenBD;
	}
	public AccountBD getAccountBD() {
		return accountBD;
	}

	public void setAccountBD(AccountBD accountBD) {
		this.accountBD = accountBD;
	}

	@RequestMapping(path="/createIncome", method = RequestMethod.POST)
	@ResponseBody
	public String createIncome(@Validated @RequestBody String reqData, 
			@RequestHeader(value="token") String token, @RequestHeader(value="userId") int userId) throws Exception {
		
		JSONObject responseobject = new JSONObject();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		JSONObject reqObject = (JSONObject) JSONObject.fromObject(reqData);
		
		int amount = 0;
		String description = null;
		Date transeDate = null;
		int accountId= 0;
		try {
			if(reqObject != null) {
				if(!reqObject.get("income_amount").equals("")) {
					amount =Integer.parseInt( (String) reqObject.get("income_amount"));
				}
				if(!reqObject.get("income_des").equals("")) {
					description = (String) reqObject.get("income_des");
				}
				if(!reqObject.get("income_date").equals("")) {
					transeDate = sdf.parse((String) reqObject.get("income_date"));
				}
				if(!reqObject.get("income_account").equals("")) {
					accountId = Integer.parseInt((String) reqObject.get("income_account"));
				}
			}
			
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				TransactionDTO transactionDTO = new TransactionDTO();
				transactionDTO.setUserId(userId);
				transactionDTO.setAmount(amount);
				transactionDTO.setDescription(description);
				transactionDTO.setTransactionType("INC");
				transactionDTO.setTransactionDate(transeDate);
				transactionDTO.setAccountId(accountId);
				TransactionDetails trans = getIncomeBD().createIncomeDetails(transactionDTO);
				
				AccountTransaction account = new AccountTransaction();
				account.setAmount(amount);
				account.setAccountId(accountId);
				account.setTransType("CR");
				account.setTransDate(transeDate);
				if(accountId >0) {
					getAccountBD().createAccountTrans(account, userId);
				}

				responseobject.put("msg", "Success");
			}
		}catch(Exception e) {
			responseobject.put("msg", e);
		}
		return responseobject.toString();
	}
	
	@RequestMapping(path="/updateIncome/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public String updateIncome(@PathVariable(value = "id") int id, @Validated @RequestBody String reqData,
			@RequestHeader(value="token") String token, @RequestHeader(value="userId") int userId) throws Exception {
		
		JSONObject responseobject = new JSONObject();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		JSONObject reqObject = (JSONObject) JSONObject.fromObject(reqData);
		
		int amount = 0;
		String description = null;
		Date transeDate = null;
		int accountId =0;
		try {
			if(!reqObject.get("income_amount").equals("")) {
				amount =Integer.parseInt( (String) reqObject.get("income_amount"));
			}
			if(!reqObject.get("income_des").equals("")) {
				String des = (String) reqObject.get("income_des");
				description = des.substring(0,1).toUpperCase() + des.substring(1);
			}
			if(!reqObject.get("income_date").equals("")) {
				transeDate = sdf.parse((String) reqObject.get("income_date"));
			}
			if(!reqObject.get("income_account").equals("")) {
				accountId = Integer.parseInt((String) reqObject.get("income_account"));
			}
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				TransactionDTO transactionDTO = new TransactionDTO();
				transactionDTO.setUserId(userId);
				transactionDTO.setAmount(amount);
				transactionDTO.setDescription(description);
				transactionDTO.setTransactionType("INC");
				transactionDTO.setTransactionDate(transeDate);
				transactionDTO.setTransactionId(id);
				if(accountId >0) {
					getAccountBD().updateAccountTrans(id, transactionDTO);
				}
				
				getIncomeBD().updateIncomeDetails(transactionDTO);
				responseobject.put("msg", "Success");
			}
		}catch(Exception e) {
			responseobject.put("msg", e);
		}
		return responseobject.toString();
	}
	
	@RequestMapping(path="/deleteIncome/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public String deleteIncome(@PathVariable(value = "id") int id,
			@RequestHeader(value="token") String token, @RequestHeader(value="userId") int userId) throws Exception {
		
		boolean isValid = getTokenBD().isTokenExist(token, userId);
		if(isValid) {
			getAccountBD().deleteAccountTrans(id);
			getIncomeBD().deleteIncomeDetails(id);
		}	
		return null;
		
	}
	
	@RequestMapping(path="/getAllIncomeDetails", method = RequestMethod.GET)
	@ResponseBody
	public String getIncomeDetails(@RequestHeader(value="token") String token, @RequestHeader(value="userId") int userId) throws Exception {
		JSONArray array =new JSONArray();
		try {
			Claims claim = GenerateToken.decodeJWT(token);
			int user =Integer.parseInt( claim.getId());
			boolean isValid = getTokenBD().isTokenExist(token, user);
			if(isValid) {
				array = getIncomeBD().getIncomeDetails(user);
			}
		}catch(SQLException e) {
        	e.printStackTrace();
        }
		
		return array.toString();
	}
	
	@RequestMapping(path="/getIncomeById", method = RequestMethod.DELETE)
	@ResponseBody
	public String getIncomeById(@RequestHeader(value="token") String token, @RequestHeader(value="userId") int userId, 
			@RequestParam int id) throws Exception {
		JSONObject object =new JSONObject();
		
		boolean isValid = getTokenBD().isTokenExist(token, userId);
		if(isValid) {
			object = getIncomeBD().getIncomeDetails(id,userId);
		}
		
		return object.toString();
		
	}

}
