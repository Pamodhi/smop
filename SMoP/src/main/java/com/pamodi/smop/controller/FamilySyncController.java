package com.pamodi.smop.controller;

import java.sql.SQLException;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pamodi.smop.bd.FamilySyncBD;
import com.pamodi.smop.bd.TokenBD;
import com.pamodi.smop.dto.FamilySyncDTO;

import net.sf.json.JSONObject;

@CrossOrigin("*")
@RestController
@RequestMapping(path = "/FamilySync")
public class FamilySyncController {
	
	private FamilySyncBD familySyncBD;
	private TokenBD tokenBD;

	public FamilySyncBD getFamilySyncBD() {
		return familySyncBD;
	}
	public void setFamilySyncBD(FamilySyncBD familySyncBD) {
		this.familySyncBD = familySyncBD;
	}
	
	public TokenBD getTokenBD() {
		return tokenBD;
	}
	public void setTokenBD(TokenBD tokenBD) {
		this.tokenBD = tokenBD;
	}
	@Autowired
    private JavaMailSender mailSender;
	
	@RequestMapping(path="/createGroup", method = RequestMethod.POST)
	@ResponseBody
	public String createGroup(@Validated @RequestBody String reqData, 
			@RequestHeader(value="userId") int userId, @RequestHeader(value="token") String token) throws Exception {
		
		JSONObject responseobject = new JSONObject();
		JSONObject reqObject = (JSONObject) JSONObject.fromObject(reqData);
		
		String groupName =null;
		String groupCode = null;
		String description = null;
		
		try {
			if(!reqObject.get("gr_name").equals("")) {
				groupName = (String) reqObject.get("gr_name");
			}
			if(!reqObject.get("gr_code").equals("")) {
				groupCode = (String) reqObject.get("gr_code");
			}
			if(!reqObject.get("gr_des").equals("")) {
				description = (String) reqObject.get("gr_des");
			}
			
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				FamilySyncDTO family = new FamilySyncDTO();
				family.setDescription(description);
				family.setGroupCode(groupCode);
				family.setGroupName(groupName);
				family.setUserId(userId);
				getFamilySyncBD().createGroup(family);
				responseobject.put("msg", "Success");
			}else {
				responseobject.put("msg", "invalidUser");
			}
		}catch(Exception e) {
			responseobject.put("msg", e);
		}
		
		return null;
		
	}
	
	@RequestMapping(path="/joinGroup", method = RequestMethod.POST)
	@ResponseBody
	public String joinGroup(@Validated @RequestBody String reqData, 
			@RequestHeader(value="userId") int userId, @RequestHeader(value="token") String token) throws Exception {
		
		JSONObject responseobject = new JSONObject();
		JSONObject reqObject = (JSONObject) JSONObject.fromObject(reqData);
		
		String groupName =null;
		String groupCode = null;
		
		try {
			groupName = ((String) reqObject.get("group_name")).trim();
			groupCode = ((String) reqObject.get("group_code")).trim();
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				boolean isGroupExist =getFamilySyncBD().checkgroupExist(groupCode, groupName, userId);
				if(isGroupExist) {
					responseobject.put("msg", "Succes");
				}else {
					responseobject.put("msg", "error");
				}
			}
			
			}catch(Exception e) {
			responseobject.put("msg", e);
		}
				return null;
		
	}
	@RequestMapping(path="/sendRequest", method = RequestMethod.POST)
	@ResponseBody
	public String addFamily(@Validated @RequestBody String reqData, 
			@RequestHeader(value="userId") int userId, @RequestHeader(value="token") String token) {
		JSONObject responseobject = new JSONObject();
		JSONObject reqObject = (JSONObject) JSONObject.fromObject(reqData);
		
		boolean isValid;
		try {
			
			String groupName =null;
			String groupCode = null;
			String email = null;
			if(!reqObject.get("grname").equals("")) {
				groupName = (String) reqObject.get("grname");
			}
			if(!reqObject.get("grid").equals("")) {
				groupCode = (String) reqObject.get("grid");
			}
			if(!reqObject.get("gremail").equals("")) {
				email = (String) reqObject.get("gremail");
			}
			
			isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				String subject="Verify Account";
			    String message="Gruop Name is = " + groupName+" Group Code is = "+groupCode;
			    String mailTo=email;
			    
			    SimpleMailMessage emailSend = new SimpleMailMessage();
			    emailSend.setTo(mailTo);
			    emailSend.setSubject(subject);
			    emailSend.setText(message);
		         
		        //sends the e-mail
		        mailSender.send(emailSend);
		        responseobject.put("msg", "Succes");
			}
			
		} catch (Exception e) {
			responseobject.put("msg", "error");
		}
		return responseobject.toString();
		
	}
	
	@RequestMapping(path="/getAllTransactions", method = RequestMethod.GET)
	@ResponseBody
	public String addFamily(@RequestHeader(value="userId") int userId, @RequestHeader(value="token") String token) throws Exception{
		JSONArray array =new JSONArray();
		try {
			boolean isValid = getTokenBD().isTokenExist(token, userId);
			if(isValid) {
				array = getFamilySyncBD().getAlltransactions(userId);
				
			}
		}catch(SQLException e) {
        	e.printStackTrace();
        }
		return array.toString();
		
	}
	
	

}
