package com.pamodi.smop.controller;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import net.sf.json.*;

import com.pamodi.smop.bd.PayeeBD;
import com.pamodi.smop.bd.TokenBD;
import com.pamodi.smop.domain.Payee;
import com.pamodi.smop.dto.PayeeDTO;

@CrossOrigin("*")
@RestController
@RequestMapping(path = "/PayeeManager")
public class PayeeController {
	
	private PayeeBD payeeBD;
	private TokenBD tokenBD;
	public PayeeBD getPayeeBD() {
		return payeeBD;
	}
	public void setPayeeBD(PayeeBD payeeBD) {
		this.payeeBD = payeeBD;
	}
	
	public TokenBD getTokenBD() {
		return tokenBD;
	}
	public void setTokenBD(TokenBD tokenBD) {
		this.tokenBD = tokenBD;
	}
	@RequestMapping(path="/createPayee", method = RequestMethod.POST)
	@ResponseBody
	public String createIncome(@Validated @RequestBody String reqData,
			@RequestHeader(value="userId") int userId, @RequestHeader(value="token") String token) throws Exception {
		JSONObject responseobject = new JSONObject();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		JSONObject reqObject = (JSONObject) JSONObject.fromObject(reqData);
		
		int account = 0;
		String name = null; 
		String email = null;
		int phone = 0;
		
		try {
			if(!reqObject.get("payee_name").equals("")) {
				name = (String) reqObject.get("payee_name");
			}
			if(!reqObject.get("payee_account").equals("")) {
				account =Integer.parseInt( (String) reqObject.get("payee_account"));
			}
			if(!reqObject.get("payee_phone").equals("")) {
				phone =Integer.parseInt( (String) reqObject.get("payee_phone"));
			}
			if(!reqObject.get("payee_email").equals("")) {
				email = (String) reqObject.get("payee_email");
			}
			
			PayeeDTO payee = new PayeeDTO();
			payee.setUserId(1);
			payee.setPayeeName(name);
			payee.setAccountNo(account);
			payee.setPhoneNo(phone);
			payee.setEmail(email);
			
			getPayeeBD().createPayee(payee);
			
			responseobject.put("msg", "Success");
		}catch(Exception e) {
			responseobject.put("msg", e);
		}
		return responseobject.toString();
	}
	
	@RequestMapping(path="/getPayee", method = RequestMethod.GET)
	@ResponseBody
	public String getPayee(@RequestHeader(value="userId") int userId, @RequestHeader(value="token") String token) throws Exception {
		JSONArray array =new JSONArray();
		try {
			array = getPayeeBD().getPayee(userId);
		}catch(SQLException e) {
        	e.printStackTrace();
        }
		
		return array.toString();
	}
	
	@RequestMapping(path="/getPayeeById", method = RequestMethod.GET)
	@ResponseBody
	public String getPayeeById(@RequestHeader(value="userId") int userId, @RequestHeader(value="token") String token,
			@PathVariable(value = "id") int id) throws Exception {
		JSONArray array =new JSONArray();
		try {
			array =getPayeeBD().getPayeeTransDetails(id);
		}catch(SQLException e) {
        	e.printStackTrace();
        }
		
		return array.toString();
	}

}
