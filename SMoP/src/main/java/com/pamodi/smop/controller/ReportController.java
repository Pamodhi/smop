package com.pamodi.smop.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pamodi.smop.bd.ReportBD;
import com.pamodi.smop.bd.TokenBD;


@CrossOrigin("*")
@RestController
@RequestMapping(path = "/ReportData")
public class ReportController {
	
	private ReportBD reportBD;
	private TokenBD tokenBD;
	
	public ReportBD getReportBD() {
		return reportBD;
	}
	public void setReportBD(ReportBD reportBD) {
		this.reportBD = reportBD;
	}
	public TokenBD getTokenBD() {
		return tokenBD;
	}
	public void setTokenBD(TokenBD tokenBD) {
		this.tokenBD = tokenBD;
	}

	@RequestMapping(path="/getBudget", method = RequestMethod.GET)
	@ResponseBody
	public String getBudget(@RequestParam(name = "toDate") String toDate, @RequestParam(name = "fromDate") String fromDate,
			@RequestHeader(value="token") String token, @RequestHeader(value="userId") int userId) throws Exception {
		
		JSONArray responseobject = new JSONArray ();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Date to_date =null;
		Date from_date = null;
		
		 to_date = sdf.parse(toDate); 
		 from_date = sdf.parse(fromDate);
		 boolean isValid = getTokenBD().isTokenExist(token, userId); 
		 if(isValid) {
			 responseobject = getReportBD().getBudget(to_date, from_date, userId); 
		 }
		 
		return responseobject.toString();
		
	}
	
	@RequestMapping(path="/getAccountTrans", method = RequestMethod.GET)
	@ResponseBody
	public String getAccountTrans(@RequestParam(name = "toDate") String toDate, @RequestParam(name = "fromDate") String fromDate,@RequestParam(name = "account") int id,
			@RequestHeader(value="token") String token, @RequestHeader(value="userId") int userId) throws Exception {
		
		JSONArray responseobject = new JSONArray ();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Date to_date =null;
		Date from_date = null;
		
		 to_date = sdf.parse(toDate); 
		 from_date = sdf.parse(fromDate);
		 boolean isValid = getTokenBD().isTokenExist(token, userId); 
		 if(isValid) {
			 responseobject = getReportBD().getAccountTrans(to_date, from_date, id);
		 }
		
		return responseobject.toString();
		
	}
	
	@RequestMapping(path="/getExpenses", method = RequestMethod.GET)
	@ResponseBody
	public String getExpenses(@RequestParam(name = "toDate") String toDate, @RequestParam(name = "fromDate") String fromDate,
			@RequestHeader(value="token") String token, @RequestHeader(value="userId") int userId) throws Exception {
		
		JSONArray responseobject = new JSONArray ();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Date to_date =null;
		Date from_date = null;
		
		 to_date = sdf.parse(toDate); 
		 from_date = sdf.parse(fromDate);
		 boolean isValid = getTokenBD().isTokenExist(token, userId); 
		 if(isValid) {
			 responseobject = getReportBD().getExpenses(to_date, from_date, userId);
		 }
		
		return responseobject.toString();
		
	}
	
	@RequestMapping(path="/getIncome", method = RequestMethod.GET)
	@ResponseBody
	public String getIncome(@RequestParam(name = "toDate") String toDate, @RequestParam(name = "fromDate") String fromDate,
			@RequestHeader(value="token") String token, @RequestHeader(value="userId") int userId) throws Exception {
		
		JSONArray responseobject = new JSONArray ();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Date to_date =null;
		Date from_date = null;
		
		 to_date = sdf.parse(toDate); 
		 from_date = sdf.parse(fromDate);
		 boolean isValid = getTokenBD().isTokenExist(token, userId); 
		 if(isValid) {
			 responseobject = getReportBD().getIncome(to_date, from_date, userId); 
		 }
		
		return responseobject.toString();
		
	}

}
