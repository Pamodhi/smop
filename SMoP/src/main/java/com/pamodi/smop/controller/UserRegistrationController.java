package com.pamodi.smop.controller;

import java.sql.SQLException;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pamodi.smop.bd.LoginBD;
import com.pamodi.smop.bd.TokenBD;
import com.pamodi.smop.bd.UserRegistrationBD;
import com.pamodi.smop.bean.GenarateCode;
import com.pamodi.smop.bean.GenerateToken;
import com.pamodi.smop.domain.UserDetails;
import com.pamodi.smop.dto.UserDetailsDTO;
import com.pamodi.smop.dto.UserLoginDTO;

import net.sf.json.JSONObject;

@CrossOrigin("*")
@Controller
@RequestMapping(path = "/ProfilesManager")
public class UserRegistrationController {
	
	private UserRegistrationBD userRegistrationBD;
	private LoginBD loginBD;
	private TokenBD tokenBD;
	
	@Autowired
    private JavaMailSender mailSender;
	
	public UserRegistrationBD getUserRegistrationBD() {
		return userRegistrationBD;
	}
	public void setUserRegistrationBD(UserRegistrationBD userRegistrationBD) {
		this.userRegistrationBD = userRegistrationBD;
	}
	public LoginBD getLoginBD() {
		return loginBD;
	}
	public void setLoginBD(LoginBD loginBD) {
		this.loginBD = loginBD;
	}
	public TokenBD getTokenBD() {
		return tokenBD;
	}
	public void setTokenBD(TokenBD tokenBD) {
		this.tokenBD = tokenBD;
	}
	@RequestMapping(path="/createUser", method = RequestMethod.POST)
	@ResponseBody
	public String createUser(@Validated @RequestBody String reqData) throws Exception {
		
		JSONObject responseobject = new JSONObject();
		JSONObject reqObject = (JSONObject) JSONObject.fromObject(reqData);
		
		String fName=null;
		String lName=null;
		String nic=null;
		String email=null;
		String password=null;
		
		try {
			if(!reqObject.get("fname").equals("")) {fName = (String) reqObject.get("fname");}
			if(!reqObject.get("lname").equals("")) {lName = (String) reqObject.get("lname");}
			if(!reqObject.get("nic").equals("")) {nic = (String) reqObject.get("nic");}
			if(!reqObject.get("email").equals("")) {email = (String) reqObject.get("email");}
			if(!reqObject.get("password").equals("")) {password = (String) reqObject.get("password");}
			
			int code = GenarateCode.userVerificationCode();
			//String passEncypt = TrippleDes.encrypt(password);
			
			boolean userExist = getUserRegistrationBD().checkUserExist(nic);
			if(!userExist) {
				UserDetailsDTO userDetailsDTO =new UserDetailsDTO();
			    userDetailsDTO.setFname(fName);
			    userDetailsDTO.setLname(lName);
			    userDetailsDTO.setNic(nic);
			    userDetailsDTO.setEmail(email);
			    userDetailsDTO.setCode(code);
			        
			    int userId = getUserRegistrationBD().insertProfile(userDetailsDTO);
			    if(userId>0) {
			    	UserLoginDTO userlog = new UserLoginDTO();
			    	userlog.setEmail(email);
			    	userlog.setUserPassword(password);
			    	userlog.setUserId(userId);
			    	userlog.setUserAccessTypeId(1);
			    	getLoginBD().insertLoginDetails(userlog);
			    }
				
			    String subject="Verify Account";
			    String message="OTP =" + code;
			    String mailTo=email;
			    
			    SimpleMailMessage emailSend = new SimpleMailMessage();
			    emailSend.setTo(mailTo);
			    emailSend.setSubject(subject);
			    emailSend.setText(message);
			     
			    // sends the e-mail
			    mailSender.send(emailSend);
			    responseobject.put("msg", "Success");
			    responseobject.put("userId",userId);
			}else {
				responseobject.put("msg", "Error");
			}
	     }catch(SQLException e) {
	        e.printStackTrace();
	     }
		return responseobject.toString();
		
	}
	
	@RequestMapping(path="/getCurrency", method = RequestMethod.POST)
	@ResponseBody
	public String getCurrency(HttpServletRequest request,HttpServletResponse response)throws Exception{
		
		JSONArray array =new JSONArray();
		JSONObject object = new JSONObject();
		try {
			array = getUserRegistrationBD().getCurrency();
			object.put("prof", array.get(0));
			object.put("crncy", array.get(1));
		}catch(SQLException e) {
        	e.printStackTrace();
        }

		return object.toString();
		
	}
	
	@RequestMapping(path="/getProfession", method = RequestMethod.POST)
	@ResponseBody
	public String getProfession(HttpServletRequest request,HttpServletResponse response)throws Exception{
		
		JSONArray array =new JSONArray();
		JSONObject object = new JSONObject();
		try {
			array = getUserRegistrationBD().getProfession();
			object.put("prof", array.get(0));
			object.put("crncy", array.get(1));
		}catch(SQLException e) {
        	e.printStackTrace();
        }

		return object.toString();
		
	}
	
	@RequestMapping(path="/userExist", method = RequestMethod.GET)
	@ResponseBody
	public String userExist(@Validated @RequestParam String nic) {
		JSONObject responseobject = new JSONObject();
		try {
			boolean userExist = getUserRegistrationBD().checkUserExist(nic);
			responseobject.put("msg", userExist);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseobject.toString();
		
	}
	
	@RequestMapping(path="/emailExist", method = RequestMethod.GET)
	@ResponseBody
	public String emailExist(@Validated @RequestParam String email) throws Exception {
		JSONObject responseobject = new JSONObject();
		try {
			int userId = getUserRegistrationBD().checkEmailExist(email);
			boolean emailExist=false;
			if(userId>0) {
				emailExist=true;
			}
			responseobject.put("msg", emailExist);
			responseobject.put("userId", userId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseobject.toString();
		
	}
	
	@RequestMapping(path="/verifyCode", method = RequestMethod.GET)
	@ResponseBody
	public String verifyAccount(@Validated @RequestParam int code, @RequestParam String email)throws Exception {
		JSONObject responseobject = new JSONObject();
		try {
			UserDetails user = getUserRegistrationBD().verifyCode(code, email);
			if(user !=null) {
				boolean verify =getUserRegistrationBD().verifyUser(user.getUserId());
				String token = GenerateToken.createJWT(user.getUserEmail(), "JWT Token",user.getFristName(), 43200000, user.getUserId());
				getLoginBD().updateToken(email, token);
				responseobject.put("msg", verify);
				responseobject.put("response", "Success");
				responseobject.put("token", token);
				responseobject.put("accessType", 1);
				responseobject.put("userId", user.getUserId());
			}else {
				responseobject.put("msg", "Please check verify code again!");
			}
		} catch (Exception e) {
			responseobject.put("msg", e);
		}
		return responseobject.toString();
		
	}

	
	@RequestMapping(path="/sendCode", method = RequestMethod.GET)
	@ResponseBody
	public String sendVefifyCode(@Validated @RequestParam String email)throws Exception {
		
		JSONObject responseobject = new JSONObject();
		try {
			int userId = getUserRegistrationBD().checkEmailExist(email);
			boolean emailExist=false;
			if(userId>0) {
				emailExist=true;
				int code = getUserRegistrationBD().getcode(userId);
						
				String subject="Verify Account";
				String message="OTP =" + code;
				String mailTo=email;
					    
				SimpleMailMessage emailSend = new SimpleMailMessage();
				emailSend.setTo(mailTo);
				emailSend.setSubject(subject);
				emailSend.setText(message);
					     
				// sends the e-mail
				mailSender.send(emailSend);
				responseobject.put("msg", emailExist);
				responseobject.put("userId", userId);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	    return responseobject.toString();
	}
	
	@RequestMapping(path="/updateUser", method = RequestMethod.PUT)
	@ResponseBody
	public String updateUser(@Validated @RequestBody String reqData)throws Exception {
		 
		return reqData;
	}
	
	@RequestMapping(path="/getUserDetails", method = RequestMethod.GET)
	@ResponseBody
	public String getUserDetails(@RequestHeader(value="token") String token, @RequestHeader(value="userId") int userId)throws Exception {
		JSONObject responseobject = new JSONObject();
		 
		boolean isValid = getTokenBD().isTokenExist(token, userId);
		if(isValid) {
			responseobject = getUserRegistrationBD().getUserDetails(userId);
		}
		return responseobject.toString();
	}
}
