package com.pamodi.smop.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "account_trans")
public class AccountTransaction {
	private int accountTransId;
	private int accountId;
	private double currentAmount;
	private double amount;
	private String transType; //DR, CR
	private Date transDate;
	
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	@Column(name="actrans_id")
	public int getAccountTransId() {
		return accountTransId;
	}
	public void setAccountTransId(int accountTransId) {
		this.accountTransId = accountTransId;
	}
	
	@Column(name="account_id")
	public int getAccountId() {
		return accountId;
	}
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}
	
	@Column(name="balance")
	public double getCurrentAmount() {
		return currentAmount;
	}
	public void setCurrentAmount(double currentAmount) {
		this.currentAmount = currentAmount;
	}
	
	@Column(name="amount")
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	@Column(name="trans_type")
	public String getTransType() {
		return transType;
	}
	public void setTransType(String transType) {
		this.transType = transType;
	}
	
	@Column(name="trans_date")
	public Date getTransDate() {
		return transDate;
	}
	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}
}
