package com.pamodi.smop.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "future_goal")
public class FutureGoal {
	private int futureGlId;
	private int userId;
	private String glName;
	private int amount;
	private Date toDate;
	private Date fromDate;
	private String note;
	private double installment;
	private int period;
	private int futRental;
	
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	@Column(name="fut_gl_id")
	public int getFutureGlId() {
		return futureGlId;
	}
	public void setFutureGlId(int futureGlId) {
		this.futureGlId = futureGlId;
	}
	
	@Column(name="user_id")
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	@Column(name="gl_name")
	public String getGlName() {
		return glName;
	}
	public void setGlName(String glName) {
		this.glName = glName;
	}
	@Column(name="amount")
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	@Column(name="toDate")
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	
	@Column(name="fromDate")
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	
	@Column(name="note")
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	@Column(name="installment")
	public double isInstallment() {
		return installment;
	}
	public void setInstallment(double installment2) {
		this.installment = installment2;
	}
	@Column(name="periods")
	public int getPeriod() {
		return period;
	}
	public void setPeriod(int period) {
		this.period = period;
	}
	@Column(name="fut_rental")
	public int getFutRental() {
		return futRental;
	}
	public void setFutRental(int futRental) {
		this.futRental = futRental;
	}

}
