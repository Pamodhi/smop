package com.pamodi.smop.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "map_fut_trans")
public class FutureGoalTransaction {
	private int fuGoalTransactionId;
	private int transactionId;
	private int futureGlId;
	
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	@Column(name="map_fut_tr_id")
	public int getFuGoalTransactionId() {
		return fuGoalTransactionId;
	}
	public void setFuGoalTransactionId(int fuGoalTransactionId) {
		this.fuGoalTransactionId = fuGoalTransactionId;
	}
	
	@Column(name="trans_id")
	public int getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}
	
	@Column(name="fut_gl_id")
	public int getFutureGlId() {
		return futureGlId;
	}
	public void setFutureGlId(int futureGlId) {
		this.futureGlId = futureGlId;
	}
	

}
