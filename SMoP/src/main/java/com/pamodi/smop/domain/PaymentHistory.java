package com.pamodi.smop.domain;

import java.util.Date;

public class PaymentHistory {
	private int payHistoryId;
	private int payId;
	private Date paidDate;
	private int amount;
	private String status;
}
