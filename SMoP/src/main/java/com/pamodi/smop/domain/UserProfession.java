package com.pamodi.smop.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "profession")
public class UserProfession {
	
	private int proffessionId;
	private String proffessionType;
	private String description;
	
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	@Column(name="prof_id")
	public int getProffessionId() {
		return proffessionId;
	}
	public void setProffessionId(int proffessionId) {
		this.proffessionId = proffessionId;
	}
	
	@Column(name="prof_type")
	public String getProffessionType() {
		return proffessionType;
	}
	public void setProffessionType(String proffessionType) {
		this.proffessionType = proffessionType;
	}
	
	@Column(name="description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	

}
