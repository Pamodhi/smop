package com.pamodi.smop.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "grp_mem_map")
public class FamilySyncMap {
	private int userId;
	private int groupId;
	private int familySyncMapId;
	private String memberType; // H-host, M-member
	
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	@Column(name="grp_mem_map_id")
	public int getFamilySyncMapId() {
		return familySyncMapId;
	}
	public void setFamilySyncMapId(int familySyncMapId) {
		this.familySyncMapId = familySyncMapId;
	}
	@Column(name="user_id")
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	@Column(name="group_id")
	public int getGroupId() {
		return groupId;
	}
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
	@Column(name="member_type")
	public String getMemberType() {
		return memberType;
	}
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

}
