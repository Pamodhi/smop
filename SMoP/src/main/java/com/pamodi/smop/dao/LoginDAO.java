package com.pamodi.smop.dao;

import com.pamodi.smop.domain.UserLoginDetails;
import com.pamodi.smop.dto.UserLoginDTO;

public interface LoginDAO {
	public UserLoginDetails getUserLoginDetail(String userEmail, String password);
	public UserLoginDetails insertLoginDetails(UserLoginDTO userloginDTO);
	public boolean checkUserExist(String email);
	public boolean updateToken(String email , String authenticationToken);
	public int userType(int userId);
	public boolean updatePassword(String password, String email);
	
	

}
