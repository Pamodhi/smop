package com.pamodi.smop.dao;

import org.json.JSONArray;

import com.pamodi.smop.domain.PaymentInfo;
import com.pamodi.smop.domain.TransactionDetails;
import com.pamodi.smop.dto.PaymentInfoDTO;
import com.pamodi.smop.dto.TransactionDTO;

import net.sf.json.JSONObject;

public interface BillPaymentDAO {
	public JSONArray getSubcategory();
	public PaymentInfo createBill(PaymentInfoDTO payInfo);
	public JSONObject getMonthlyBill(int userId);
	public void deleteBill(int id);
	public JSONArray getbillDetails(int userId);
	public void payBill(int id);

}
