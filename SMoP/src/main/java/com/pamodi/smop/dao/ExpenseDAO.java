package com.pamodi.smop.dao;

import java.util.Date;

import org.json.JSONArray;

import com.pamodi.smop.domain.PaymentInfo;
import com.pamodi.smop.domain.TransactionDetails;
import com.pamodi.smop.dto.PaymentInfoDTO;
import com.pamodi.smop.dto.TransactionDTO;

import net.sf.json.JSONObject;

public interface ExpenseDAO {
	public TransactionDetails createExpenseDetails(TransactionDTO transactionDTO);
	public void updateExpenseDetails(TransactionDTO transactionDTO);
	public void deleteExpenseDetails(int id);
	public JSONObject getExpenseDetails(int id, int userId);
	public JSONArray getExpenseDetails(int userId);
	public JSONArray getSubcategory();
	public JSONObject getMonthlyExpese(int userId);
	public JSONArray getTransactionDateRange(Date toDate, Date fromDate, int userId);
	public double getLastMonthExpense(int catId, int userId);
	public void updateFutureGoal(int userId);

}
