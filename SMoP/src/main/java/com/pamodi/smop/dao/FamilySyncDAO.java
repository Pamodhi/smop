package com.pamodi.smop.dao;

import org.json.JSONArray;

import com.pamodi.smop.domain.FamilySync;
import com.pamodi.smop.dto.FamilySyncDTO;

public interface FamilySyncDAO {
	
	public FamilySync createGroup(FamilySyncDTO family);
	public boolean checkgroupExist(String code, String name, int userId);
	public JSONArray getAlltransactions(int userId);

}
