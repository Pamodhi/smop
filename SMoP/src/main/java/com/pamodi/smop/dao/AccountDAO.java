package com.pamodi.smop.dao;

import org.json.JSONArray;

import com.pamodi.smop.domain.AccountTransaction;
import com.pamodi.smop.domain.Accounts;
import com.pamodi.smop.dto.AccountDTO;
import com.pamodi.smop.dto.TransactionDTO;

import net.sf.json.JSONObject;

public interface AccountDAO {
	public Accounts createAccount(AccountDTO accountDTO);
	public JSONArray getAccounts(int userId);
	public AccountTransaction createAccountTrans(String type, Accounts account);
	public AccountTransaction createAccountTrans(AccountTransaction account, int userId);
	public JSONObject getAccountBalance(int userId);
	public void deleteAccountTrans(int tarnsId);
	public void updateAccountTrans(int tarnsId, TransactionDTO account);
}
