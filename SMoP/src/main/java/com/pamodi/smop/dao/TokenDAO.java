package com.pamodi.smop.dao;

public interface TokenDAO {
	public boolean isTokenExist(String token, int userId);
}
