package com.pamodi.smop.dao;

import org.json.JSONArray;

import com.pamodi.smop.domain.Payee;
import com.pamodi.smop.dto.PayeeDTO;

public interface PayeeDAO {
	public Payee createPayee(PayeeDTO payee);
	public JSONArray getPayee(int userId);
	public JSONArray getPayeeTransDetails(int id);
}
