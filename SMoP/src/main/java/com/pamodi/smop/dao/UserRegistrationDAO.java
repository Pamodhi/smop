package com.pamodi.smop.dao;

import org.json.JSONArray;

import com.pamodi.smop.domain.UserDetails;
import com.pamodi.smop.dto.UserDetailsDTO;

import net.sf.json.JSONObject;

public interface UserRegistrationDAO {
	public Integer insertProfile(UserDetailsDTO userDetailsDTO);
	public JSONArray getProfession();
	public JSONArray getCurrency();
	public boolean checkUserExist(String nic);
	public Integer checkEmailExist(String email);
	public UserDetails verifyCode(int code, String email);
	public boolean verifyUser(int userId);
	public Integer getcode(int userId);
	public JSONObject getUserDetails(int userId);
	

}
