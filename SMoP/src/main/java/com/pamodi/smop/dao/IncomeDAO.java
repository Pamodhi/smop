package com.pamodi.smop.dao;

import org.json.JSONArray;

import com.pamodi.smop.domain.TransactionDetails;
import com.pamodi.smop.dto.TransactionDTO;

import net.sf.json.JSONObject;

public interface IncomeDAO {
	public TransactionDetails createIncomeDetails(TransactionDTO transactionDTO);
	public void updateIncomeDetails(TransactionDTO transactionDTO);
	public void deleteIncomeDetails(int id) ;
	public JSONObject getIncomeDetails(int transId,int userId);
	public JSONArray getIncomeDetails(int userId);
	public JSONObject getMonthlyIncome(int userId);

}
