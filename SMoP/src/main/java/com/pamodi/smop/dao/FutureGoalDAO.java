package com.pamodi.smop.dao;

import org.json.JSONArray;

import com.pamodi.smop.domain.FutureGoal;
import com.pamodi.smop.dto.FutureGoalDTO;

public interface FutureGoalDAO {
	public FutureGoal createGoal(FutureGoalDTO goalDTO);
	public JSONArray getGoalDetails(int userId);

}
