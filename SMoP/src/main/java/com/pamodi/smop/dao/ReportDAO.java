package com.pamodi.smop.dao;

import java.util.Date;

import org.json.JSONArray;

public interface ReportDAO {
	public JSONArray getBudget(Date toDate, Date FromDate, int userId);
	public JSONArray getAccountTrans(Date toDate, Date fromDate,int userId);
	public JSONArray getExpenses(Date toDate, Date fromDate,int userId);
	public JSONArray getIncome(Date toDate, Date fromDate,int userId);

}
