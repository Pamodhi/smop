package com.pamodi.smop.dao;

import org.json.JSONArray;

public interface AdminDAO {

	public String confirmUserRegistration(int userId, String status);
	public JSONArray getUserList();

}
