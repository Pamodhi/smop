<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="resources/bootstrap/js/bootstrap.min.js"/>
	<link rel="stylesheet" href="resources/css/styleLogin.css"/> 
    <title>Login</title>
    
</head>
<script src ="resources/js/jquery-3.4.1.js" type="text/javascript"></script>
<script src ="resources/js/login.js" type="text/javascript"></script>
<script type="text/javascript">

	 $(document).ready(function(){
	$(".signup-form").hide();
	$(".signup").css("background", "none");
	
	$(".login").click(function(){
			  $(".signup-form").hide();
			  $(".login-form").show();
			  $(".signup").css("background", "none");
			  $(".login").css("background", "#fff");
	});
		
	$(".signup").click(function(){
		$(".signup-form").show();
		  $(".login-form").hide();
		  $(".login").css("background", "none");
		  $(".signup").css("background", "#fff");
	});
	
	$(".btn").click(function(){
		  $(".input").val("");
	});
	
}); 


data = "";
	
	loginMe = function(){
	    $.ajax({
	        url:'LoginManager/login',
	        type:'POST',
	        data:{username:$("#username").val(),password:$('#userpass').val()},
	        dataType: "json",
	        success: function(response){
	        	if(response.msg == "Success"){
	        		alert(response.msg);
	        		if(response.loginId == 1){
	        			window.location.href = "adminDashboard.jsp";
	        		}else{
	        			window.location.href = "home.jsp";
	        		}
	        	}else{
	        		alert(response.error); 
	                window.location.href = "index.jsp";
	        	}
	                
	        }            
	    });         
	}
	
	singUp = function(){
		$.ajax({
	        url:'LoginManager/singup',
	        type:'POST',
	        data:{username:$("#username").val(),password:$('#userpass').val(), email:$('#useremail').val()},
	        dataType: "json",
	        success: function(response){
	        	if(response.msg == "Success"){
	       			alert(response.msg);
	        		window.location.href = "home.jsp";
	        	}else{
	        		alert(response.msg);
	        		clearAll();
	        	} 
	        }            
	    });
	}
	
	clearSession = function(){
		$.ajax({
			url:'LoginManager/logout',
	        type:'POST',
	        dataType: "json",
	        success: function(response){
	        	
	        }
		});
	}
	
	/* clearAll = function(){
		$("#username").val()=="";
		$('#userpass').val()=="";
		$('#useremail').val() =="";
	} */

</script>
    
<body>

<div class="wrapper">
    <div class="container">
    
      <div class="login">Log In</div>
      <div class="signup" >Sign Up</div>
      
      	<div class="login-form">
      	<form onsubmit="loginMe();">
          <input type="text" placeholder="Username" class="form-control" id="username" required="required" name="username" style=" height: 60px;"><br />
          <input type="password" placeholder="Password" class="form-control" id="userpass" required="required" name="userpass" style=" height: 60px;"><br />
          <input type="submit" class="btn" id="login" value="log in">
          <!-- <div class="btn" onclick="loginMe();">log in</div> -->
          <span><a href="#">I forgot my username or password.</a></span>
          </form>
      </div>
      
	  <div class="signup-form">
	  	<form onsubmit="singUp();">
          <input type="email" placeholder="Your Email Address" class="form-control" id="useremail" required="required" name="useremail" style=" height: 60px;" ><br />
          <input type="text" placeholder="Choose a Username" class="form-control" id="name" required="required" name="name" style=" height: 60px;"><br />
          <input type="password" placeholder="Choose a Password" class="form-control" id="password" required="required" name="password" style=" height: 60px;"><br />
          <input type="submit" class="btn" id="sign" value="Create account">
          <!-- <div class="btn" onclick="singUp();">Create account</div> -->
          </form>
       </div>
    </div>
 </div>

    
</body>
</html>