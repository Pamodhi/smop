<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
    <link rel="stylesheet" href="resources/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="resources/bootstrap/js/bootstrap.min.js"/>
<title></title>
 <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 550px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
      margin-left: 15px;
    }
    .jumbotron {
    	height:150px;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
        
      }
      .row.content {height: auto;} 
    }
    
    .tiletag{
	    text-align: center;
	    height: 50px;
	    background-color: #337ab7;
	    padding-top: 15px;
	    border-radius: 10px;
    }
  </style>
</head>

<script src ="resources/js/jquery-3.4.1.js" type="text/javascript"></script>
<script type="text/javascript">
function openWindow(path){
    		window.open(path,"","status=1,width=650px,height=450px");
		}
	function sessionkill(){
		
	} 
	
	
	getData = function(){  
	    $.ajax({
	        url:'AdminManager/getUsers',
	        type:'GET',
	        dataType: "json",
	        success: function(response){
	                data = response.data;
	                alert(data);
	                $('.tr').remove();
	                for(i=0; i<response.data.length; i++){                   
	                    /* $("#table").append("<tr class='tr'> <td> "+response.data[i][0]+" </td><td> "+response.data[i][1]+" </td><td> "+response.data[i][2]+" </td><td> "+response.data[i][3]+" </td> <td> "+response.data[i][4]+" </td><td>"+response.data[i][5]+"</td> <td> <a href='#' onclick= approve("+response.data[i][0]+");> Approve </a>  </td> </td> <td> <a href='#' onclick='reject_("+response.data[i][0]+");'> Reject </a>  </td> </tr>"); */
	                	var temp = '<tr  class="tr"  id="' +data[i][0] + '"><td>'+ data[i][1] + '</td><td> '+data[i][2]+'</td><td>'+data[i][3]+'</td><td>'+data[i][4]+'</td><td>'+data[i][5]+'</td> ';
							temp += '<td>' + '<button type="button" id="approve" onclick="approve('+data[i][0]+');">Approve</button>' + '</td><td>'+'<button type="button" id="reject" onclick="reject('+data[i][0]+');">Reject</button>'+'</td></tr>';
							$('#table').append(temp);
	                }           
	        }               
	    });
	     
	}
	
	function approve(id){
		var sts ="A";
		$.ajax({
	        url:'AdminManager/getConfirm',
	        type:'POST',
	        data:{userId:id,
	        		status:sts},
	        dataType: "json",
	        success: function(response){
	        	if(response.msg == "Success"){
	        		 window.location.href = "#";
	        	}else{
	        		clearAll();
	        	} 
	        }            
	    });
	}
	
	function reject(id){
		var sts ="R";
		$.ajax({
	        url:'AdminManager/getUsers',
	        type:'POST',
	        data:{userId:id,status:sts},
	        dataType: "json",
	        success: function(response){
	        	if(response.msg == "Success"){
	        		 window.location.href = "#";
	        	}else{
	        		clearAll();
	        	} 
	        }            
	    });
	}
	
	
</script>


<body  onload="getData();">

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      	<li onmousedown="logoutMe();"><a href="index.jsp"><span class="glyphicon glyphicon-log-in"></span> LogOut</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-2 sidenav">
      <legend><h1>SMoP</h1></legend>
      <ul class="nav nav-pills nav-stacked">
        <li><a href="adminDashboard.jsp">Dashboard</a></li>
        <li><a href="adminReports.jsp">Report</a></li>
        <li class="active"><a href="pendingRequest.jsp">Confirm Registration</a></li>
        <li><a href="#">Help</a></li>
      </ul><br>
    </div>
    <div class="col-sm-8 sidenav">
    	<table id="table" border=1 style="width:100%">
            <tr><th> First Name </th> <th> Last Name </th> <th> NIC </th> <th> Profession </th> <th> Currency </th> <th> Approve </th> <th> Reject </th> </tr>
         
    	</table>
    </div>
</div>

<footer class="container-fluid">
  <p></p>
</footer>
</body>
</html>