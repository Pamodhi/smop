<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<link rel="stylesheet" href="resources/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="resources/bootstrap/js/bootstrap.min.js"/>
<title>Add Expenses</title>
</head>

<script src ="resources/js/jquery-3.4.1.js" type="text/javascript"></script>
<script>
saveExpenseData= function(){
	$.ajax({
		url:'TransactionDetailsManager/saveExpenses',
        type:'POST',
        data:{	expense:$("#catog").val(),
        		date:$('#date').val(),
        		amount:$("#amount").val(),
        		note:$("#note").val()
        	},
        dataType: "json",
        success: function(response){
        	alert("create success")
        	window.location.href = "home.jsp";
        }
	});
}

$(document).ready(function(){
	$.ajax({
		url:'TransactionDetailsManager/getDetails',
        type:'POST',
        dataType: "json",
        success: function(response){
        	alert(response.catog);
        	setValues("catog",response.catog);//subcategory
        },
	});
});


function setValues(selectId, data){
	var selectSet = document.getElementById(selectId);
	
	for (var i = 0; i < data.length; i++) {
        var option = document.createElement("OPTION");

        //Set Customer Name in Text part.
        option.innerHTML = data[i][1];

        //Set CustomerId in Value part.
        option.value = data[i][0];

        //Add the Option element to DropDownList.
        selectSet.options.add(option);
    }
}

</script>

<body>
	<div class="container-fluid">
	  	<div class="row content">
	  	<div class="col-sm-6">
	  	<form onsubmit="saveExpensesData();">
		<legend> Add Expenses </legend>
	    <div  class="form-group">
	      <label for="expenses">Sub Category:</label>
	       <select class="form-control" name="catog" id="catog" required="required"></select>
	    </div>
		<div  class="form-group">
	      <label for="date">Date:</label>
	      <input type="date" class="form-control" id="date" required="required" placeholder="" name="date">
	    </div>
	    <div  class="form-group">
	      <label for="amount">Amount:</label>
	      <input type="text" class="form-control" id="amount" required="required" placeholder="Amount" name="amount">
	    </div>
	    <div  class="form-group">
	      <label for="note">Notes:</label>
	      <input type="text" class="form-control" id="note" required="" placeholder="Optional" name="note">
	    </div>
	    <div class="checkbox">
	      <label><input type="hidden" name="remember" type="hidden"> </label>
	    </div>
	    <button type="submit" class="btn btn-default">Save</button>
	    </form>
	  </div>
	  
	  </div>
	</div>
</body>
</html>