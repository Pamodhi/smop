<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="resources/bootstrap/css/bootstrap.css"/>
<link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css"/>
<link rel="stylesheet" href="resources/bootstrap/js/bootstrap.min.js"/>

<title>SMoP</title>
<style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 100%}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height: auto;} 
    }
  </style>
</head>
<script src ="resources/js/jquery-3.4.1.js" type="text/javascript"></script>
<script type="text/javascript">
	//function $(name){return document.getElementById(name);}

	insertData= function(){
		$.ajax({
			url:'ProfilesManager/createUser',
	        type:'POST',
	        data:{	fname:$("#fname").val(),
	        		lname:$('#lname').val(),
	        		email:$("#email").val(),
	        		nic:$("#nic").val().trim(),
	        		prof:$("#prof").val(),
	        		crncy:$("#crncy").val()
	        	},
	        dataType: "json",
	        success: function(response){
	        	alert("create success")
	        	window.location.href = "home.jsp";
	        }
		});
	}
	
	$(document).ready(function(){
		$.ajax({
			url:'ProfilesManager/getDetails',
	        type:'POST',
	        dataType: "json",
	        success: function(response){
	        	setValues("prof",response.prof);//Profession
				setValues("crncy",response.crncy);//currency
	        },
		});
	});

 
function setValues(selectId, data){
	var selectSet = document.getElementById(selectId);
	
	for (var i = 0; i < data.length; i++) {
        var option = document.createElement("OPTION");

        //Set Customer Name in Text part.
        option.innerHTML = data[i][1];

        //Set CustomerId in Value part.
        option.value = data[i][0];

        //Add the Option element to DropDownList.
        selectSet.options.add(option);
    }
}


$(document).ready(function() {
	var a = new XMLHttpRequest();
	a.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var data = JSON.parse(this.responseText);
			//alert(data);
			setValues("prof",data[1]);//Profession
			setValues("crncy",data[0]);//Currency
		}
	};
	a.open("GET", "ProfileManager?method=2", true);
	a.send();
}); 
</script>

<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <!-- <a class="navbar-brand" href="#">Logo</a> -->
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="home.jsp">Home</a></li>
        <li><a href="reports.jsp">Report</a></li>
        <li><a href="contactUs.jsp">Contact</a></li>
      </ul>
      
    </div>
  </div>
</nav>

<div class="container-fluid">
  	<div class="row content" style="height: 555px;padding-top: 50p;padding-bottom: 19px;">
  		<div class="col-sm-4 sidenav"></div>
       <div class="col-sm-4 sidenav" style="padding-top:50px;">
			<%-- <legend><h1>Hi <%=session.getAttribute("name")%> </h1></legend> --%>
			<form onsubmit="insertData();">
			    <div  class="form-group">
			      <label for="fname">First Name:</label>
			      <input type="text" class="form-control" id="fname" required="required" placeholder="Enter first name" name="fname">
			    </div>
			    <div class="form-group">
			      <label for="lname">Last Name:</label>
			      <input type="text" class="form-control" id="lname" required="required" placeholder="Enter last name" name="lname">
			    </div>
			    <div class="form-group">
			      <label for="nic">NIC:</label>
			      <input type="text" class="form-control" id="nic" required="required" placeholder="Enter nic" name="nic">
			    </div>
			   	<div class="form-group">
			      <label for="prof">Profession:</label><font style="color:red">*</font>
			      <select class="form-control" name="prof" id="prof" required="required"></select>
			      
			    </div>
			    <div class="form-group">
			      <label for="crncy">Currency:</label><font style="color:red">*</font>
			      <select class="form-control" name="crncy" id="crncy" required="required"></select>
			      
			    </div>
			    <button type="submit" class="btn btn-default">Submit</button>
			 </form>
       </div>
       <div class="col-sm-4 sidenav"></div>
	</div>
</div>
<footer class="container-fluid">
  <p></p>
</footer>
</body>
</html>