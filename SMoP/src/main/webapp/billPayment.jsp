<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<link rel="stylesheet" href="resources/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="resources/bootstrap/js/bootstrap.min.js"/>
<title>Add Expenses</title>
</head>

<script>

</script>

<body>
	<div class="container-fluid">
  	<div class="row content">
	  	<div class="col-sm-6">
	  	<form onsubmit="saveIncomeData();">
			<legend> Add Income </legend>
			    <div  class="form-group">
			      <label for="username">Desciption:</label>
			      <input type="text" class="form-control" id="income" required="required" placeholder="income" name="income">
			    </div>
				<div  class="form-group">
			      <label for="username">Date:</label>
			      <input type="date" class="form-control" id="date" required="required" placeholder="" name="date">
			    </div>
			    <div  class="form-group">
			      <label for="username">Amount:</label>
			      <input type="text" class="form-control" id="amount" required="required" placeholder="" name="amount">
			    </div>
			    <div  class="form-group">
			      <label for="username">Notes:</label>
			      <input type="text" class="form-control" id="note" required="" placeholder="Optional" name="note">
			    </div>
			    <div class="checkbox">
			      <label><input type="hidden" name="remember" type="hidden"> </label>
			    </div>
			    <button type="submit" class="btn btn-default">Save</button>
			</form>
	  </div>
  </div>
</div>
</body>
</html>