<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
    <link rel="stylesheet" href="resources/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="resources/bootstrap/js/bootstrap.min.js"/>
<title>Help</title>
 <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 550px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
      margin-left: 15px;
    }
    .jumbotron {
    	height:150px;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
        
      }
      .row.content {height: auto;} 
    }
    
    .tiletag{
	    text-align: center;
	    height: 50px;
	    background-color: #337ab7;
	    padding-top: 15px;
	    border-radius: 10px;
    }
  </style>
</head>

<script src ="resources/js/jquery-3.4.1.js" type="text/javascript"></script>
<script src ="resources/js/login.js" type="text/javascript"></script>
<script type="text/javascript">
	function openWindow(path){
    		window.open(path,"","status=1,width=650px,height=450px");
		}
	function sessionkill(){
		
	}
	
	
	logoutMe = function(){
		$.ajax({
			url:'LoginManager/logout',
	        type:'POST',
	        dataType: "json",
	        success: function(response){
	        	alert(response.logout);
	        	window.location.href = "index.jsp";
	        }
		});
	}
</script>


<body >

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#"></a></li>
        <li><a href="#"></a></li>
        <li><a href="#"></a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      	<li onmousedown="logoutMe();"><a href="index.jsp"><span class="glyphicon glyphicon-log-in"></span> LogOut</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-2 sidenav">
      <legend><h1>SMoP</h1></legend>
      <ul class="nav nav-pills nav-stacked">
        <li><a href="home.jsp">Dashboard</a></li>
        <li><a href="reports.jsp">Report</a></li>
        <li class="active"><a href="contactUs.jsp">Help</a></li>
      </ul><br>
    </div>
</div>

<footer class="container-fluid">
  <p></p>
</footer>
</body>
</html>